"use strict";

const { Menu } = require("electron");
const electron = require("electron");
const app = electron.app;
const openAboutWindow = require("about-window").default;
const path = require("path");

const template = [
	{
		label: "Edit",
		submenu: [
			{
				role: "undo"
			},
			{
				role: "redo"
			},
			{
				type: "separator"
			},
			{
				role: "cut"
			},
			{
				role: "copy"
			},
			{
				role: "paste"
			},
			{
				role: "pasteandmatchstyle"
			},
			{
				role: "delete"
			},
			{
				role: "selectall"
			}
		]
	},
	{
		label: "View",
		submenu: [
			{
				role: "resetzoom"
			},
			{
				role: "zoomin"
			},
			{
				role: "zoomout"
			},
			{
				type: "separator"
			},
			{
				role: "togglefullscreen"
			}
		]
	},
	{
		role: "window",
		submenu: [
			{
				role: "minimize"
			},
			{
				role: "close"
			}
		]
	},
	{
		role: "help",
		submenu: [
			{
				label: "Learn More",
				click() {
					require("electron").shell.openExternal(
						"https://www.narnoo.com/"
					);
				}
			}
		]
	}
];

let buildMenu = () => {
	if (process.platform === "darwin") {
		const name = app.getName();
		const aboutPath = path.join(__dirname, "../assets/icons/png/256x256.png");
		console.log("aboutPath: ", aboutPath);

		template.unshift({
			label: name,
			submenu: [
				{
					label: "About Narnoo Admin App",
					click: () => {
						openAboutWindow({
							icon_path: aboutPath,
							copyright: "Copyright (c) 2018",
							package_json_dir: path.join(__dirname, "../"),
						});
					}
				},
				{
					type: "separator"
				},
				{
					role: "services",
					submenu: []
				},
				{
					type: "separator"
				},
				{
					role: "hide"
				},
				{
					role: "hideothers"
				},
				{
					role: "unhide"
				},
				{
					type: "separator"
				},
				{
					role: "quit"
				}
			]
		});

		// Edit menu.
		template[1].submenu.push(
			{
				type: "separator"
			},
			{
				label: "Speech",
				submenu: [
					{
						role: "startspeaking"
					},
					{
						role: "stopspeaking"
					}
				]
			}
		);

		// Window menu.
		template[3].submenu = [
			{
				label: "Close",
				accelerator: "CmdOrCtrl+W",
				role: "close"
			},
			{
				label: "Minimize",
				accelerator: "CmdOrCtrl+M",
				role: "minimize"
			},
			{
				label: "Zoom",
				role: "zoom"
			},
			{
				type: "separator"
			},
			{
				label: "Bring All to Front",
				role: "front"
			}
		];
	}

	const menu = Menu.buildFromTemplate(template);
	Menu.setApplicationMenu(menu);
};

module.exports = {
	buildMenu: buildMenu
};
