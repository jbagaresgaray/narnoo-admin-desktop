import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import * as _ from "lodash";

@Injectable({
  providedIn: "root"
})
export class AuthAppGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.checkLogin();
  }

  checkLogin(): boolean {
    const token = localStorage.getItem("app.admintoken");
    if (!_.isEmpty(token)) {
      this.router.navigate(["/home"]);
      return true;
    }

    // Navigate to the login page with extras
    return true;
  }
}
