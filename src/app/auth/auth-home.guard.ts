import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import * as _ from "lodash";

import { LoginService } from "../services/login.service";

@Injectable({
  providedIn: "root"
})
export class AuthHomeGuard implements CanActivate {
  constructor(private authService: LoginService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.checkLogin();
  }

  checkLogin(): boolean {
    const token = localStorage.getItem("app.admintoken");
    if (!_.isEmpty(token)) {
      return true;
    }

    if (this.authService.isLoggedIn) {
      return true;
    }

    // Navigate to the login page with extras
    this.router.navigate(["/login"]);
    return false;
  }
}
