import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  BsDropdownModule,
  CollapseModule,
  TabsModule,
  ModalModule,
  AccordionModule,
  PaginationModule,
  TooltipModule
} from "ngx-bootstrap";
import { NgxCoolDialogsModule } from "ngx-cool-dialogs";
import { ContextMenuModule } from "ngx-contextmenu";
import { LoadingBarHttpClientModule } from "@ngx-loading-bar/http-client";
import { LoadingBarHttpModule } from "@ngx-loading-bar/http";
import { LoadingBarModule, LoadingBarService } from "@ngx-loading-bar/core";
import { ToastrModule } from "ngx-toastr";
import { RestangularModule, Restangular } from "ngx-restangular";
import {
  DropzoneModule,
  DropzoneConfigInterface,
  DROPZONE_CONFIG
} from "ngx-dropzone-wrapper";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { ClipboardModule } from "ngx-clipboard";
import { NgxBreadcrumbsModule } from "@nivans/ngx-breadcrumbs";
import { TagInputModule } from "ngx-chips";
import { NgxElectronModule } from "ngx-electron";
import { BroadcasterService } from "ng-broadcaster";
import { NgSelectModule } from "@ng-select/ng-select";
import { DndModule } from "ngx-drag-drop";

import { ROUTES } from "./app-routing.module";
import { DashboardModule } from "./pages/dashboard/dashboard.module";
import { LoginModule } from "./pages/login/login.module";
import { MediaModule } from "./pages/media/media.module";
import { HomeModule } from "./pages/home/home.module";
import { CompanyBioModule } from "./pages/company-bio/company-bio.module";
import { ProductsModule } from "./pages/products/products.module";
import { UsersModule } from "./pages/users/users.module";
import { SocialLinksModule } from "./pages/social-links/social-links.module";
import { RegisterModule } from "./pages/register/register.module";
import { ForgotModule } from "./pages/forgot/forgot.module";
import { UploadModule } from "./pages/upload/upload.module";
import { DatapickerModule } from "./pages/datapicker/datapicker.module";
import { UserProfileModule } from "./pages/user-profile/user-profile.module";

import { ComponentsModule } from "./components/components.module";
import { PipesModule } from "./pipes/pipes.module";

import { AppComponent } from "./app.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { LayoutComponent } from "./pages/layout/layout.component";

import { environment } from "../environments/environment";

const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(ROUTES);

export function RestangularConfigFactory(
  RestangularProvider,
  loader: LoadingBarService
) {
  RestangularProvider.setBaseUrl(
    environment.api_url + environment.api_admin_version
  );
  RestangularProvider.addFullRequestInterceptor(() => loader.start());
  RestangularProvider.addErrorInterceptor(() => loader.complete());
  RestangularProvider.addResponseInterceptor(data => {
    loader.complete();
    return data || {};
  });
}

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  url: environment.api_url + environment.api_version,
  createImageThumbnails: true,
  autoProcessQueue: false,
  maxFiles: 10,
  parallelUploads: 10,
  addRemoveLinks: true,
  method: "POST",
  timeout: 3600000
};

declare let $: any;
declare let jQuery: any;

@NgModule({
  declarations: [AppComponent, DashboardComponent, LayoutComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ComponentsModule,
    PipesModule,
    AppRoutingModule,
    DashboardModule,
    LoginModule,
    HomeModule,
    MediaModule,
    CompanyBioModule,
    ProductsModule,
    UsersModule,
    SocialLinksModule,
    RegisterModule,
    ForgotModule,
    UploadModule,
    DatapickerModule,
    UserProfileModule,
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    PaginationModule.forRoot(),
    TooltipModule.forRoot(),
    ContextMenuModule.forRoot({
      useBootstrap4: true
    }),
    NgxCoolDialogsModule.forRoot(),
    ToastrModule.forRoot(),
    LoadingBarModule.forRoot(),
    // LoadingBarHttpClientModule,
    // LoadingBarHttpModule,
    RestangularModule.forRoot([LoadingBarService], RestangularConfigFactory),
    NgxBreadcrumbsModule,
    TagInputModule,
    HttpClientModule,
    DropzoneModule,
    FormsModule,
    ClipboardModule,
    InfiniteScrollModule,
    NgxElectronModule,
    NgSelectModule,
    DndModule
  ],
  providers: [
    BroadcasterService,
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
