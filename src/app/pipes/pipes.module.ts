import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LinkHttpPipe } from "./link-http.pipe";
import { FileSizePipe } from "./file-size.pipe";
import { RelativeTimePipe } from "./relative-time.pipe";
import { TimesagoPipe } from "./timesago.pipe";

@NgModule({
  declarations: [LinkHttpPipe, FileSizePipe, RelativeTimePipe, TimesagoPipe],
  imports: [CommonModule],
  exports: [LinkHttpPipe, FileSizePipe, RelativeTimePipe, TimesagoPipe],
})
export class PipesModule {}
