import { Injectable, Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

/**
 * Generated class for the TimesagoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: "timesago"
})
@Injectable()
export class TimesagoPipe implements PipeTransform {
  now: any;

  transform(value: string, args) {
    this.now = moment(value).fromNow();
    return this.now;
  }
}
