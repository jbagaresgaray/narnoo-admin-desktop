import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "./auth/auth.guard";
import { AuthHomeGuard } from "./auth/auth-home.guard";
import { AuthAppGuard } from "./auth/auth-app.guard";

import { LayoutComponent } from "./pages/layout/layout.component";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { ForgotComponent } from "./pages/forgot/forgot.component";
import { HomeComponent } from "./pages/home/home.component";
import { CreateOperatorComponent } from "./pages/home/create-operator/create-operator.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { MediaComponent } from "./pages/media/media.component";
import { ImageDetailComponent } from "./pages/media/images/image-detail/image-detail.component";
import { LogoDetailComponent } from "./pages/media/logo/logo-detail/logo-detail.component";
import { PrintDetailComponent } from "./pages/media/print/print-detail/print-detail.component";
import { VideoDetailComponent } from "./pages/media/video/video-detail/video-detail.component";
import { ProductsComponent } from "./pages/products/products.component";
import { ProductDetailsComponent } from "./pages/products/product-details/product-details.component";
import { ImportProductComponent } from "./pages/products/import-product/import-product.component";
import { ImportProductDetailsComponent } from "./pages/products/import-product-details/import-product-details.component";
import { CompanyBioComponent } from "./pages/company-bio/company-bio.component";
import { UsersComponent } from "./pages/users/users.component";
import { UserDetailsComponent } from "./pages/users/user-details/user-details.component";
import { SocialLinksComponent } from "./pages/social-links/social-links.component";
import { UserProfileComponent } from "./pages/user-profile/user-profile.component";

export const ROUTES: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "login",
    component: LoginComponent,
    data: {
      class: "o-page--center"
    }
  },
  {
    path: "register",
    component: RegisterComponent,
    data: {
      class: "o-page--center"
    }
  },
  {
    path: "forgot",
    component: ForgotComponent,
    data: {
      class: "o-page--center"
    }
  },
  {
    path: "home",
    canActivateChild: [AuthHomeGuard],
    component: HomeComponent,
    data: {
      class: "o-page"
    }
  },
  {
    path: "create",
    canActivateChild: [AuthHomeGuard],
    component: CreateOperatorComponent,
    data: {
      class: "o-page"
    }
  },
  {
    path: "",
    component: LayoutComponent,
    data: {
      class: "o-page__content",
      breadcrumb: "Dashboard",
      isHome: true,
      icon: "fa fa-home",
      show: false
    },
    canActivate: [AuthHomeGuard],
    children: [
      {
        path: "dashboard",
        canActivateChild: [AuthHomeGuard],
        component: DashboardComponent,
        data: {
          name: "dashboard",
          class: "o-page",
          title: "Dashboard",
          breadcrumb: "Dashboard",
          isHome: false
        }
      },
      {
        path: "user-profile",
        canActivateChild: [AuthHomeGuard],
        component: UserProfileComponent,
        data: {
          name: "user-profile",
          class: "o-page",
          title: "Business Profile"
        }
      },
      {
        path: "media",
        component: MediaComponent,
        data: {
          name: "media",
          class: "o-page",
          title: "Media Files",
          hasSearchbar: true
        }
      },
      {
        path: "image-details",
        component: ImageDetailComponent,
        data: {
          name: "image-details",
          class: "o-page",
          title: "Image Detail",
          hasSearchbar: false
        }
      },
      {
        path: "logo-details",
        component: LogoDetailComponent,
        data: {
          name: "logo-details",
          class: "o-page",
          title: "Logo Detail",
          hasSearchbar: false
        }
      },
      {
        path: "print-details",
        component: PrintDetailComponent,
        data: {
          name: "print-details",
          class: "o-page",
          title: "Brochure Detail",
          hasSearchbar: false
        }
      },
      {
        path: "video-details",
        component: VideoDetailComponent,
        data: {
          name: "video-details",
          class: "o-page",
          title: "Video Detail",
          hasSearchbar: false
        }
      },
      {
        path: "company-bio",
        canActivateChild: [AuthHomeGuard],
        component: CompanyBioComponent,
        data: {
          name: "company-bio",
          class: "o-page",
          title: "Company Bio",
          hasSearchbar: false
        }
      },
      {
        path: "products",
        canActivateChild: [AuthHomeGuard],
        component: ProductsComponent,
        data: {
          name: "products",
          class: "o-page",
          title: "Products",
          breadcrumb: "Products",
          icon: "fa fa-shopping-cart",
          isHome: false,
          hasSearchbar: true
        }
      },
      {
        path: "import-products",
        canActivateChild: [AuthHomeGuard],
        component: ImportProductComponent,
        data: {
          name: "import-products",
          class: "o-page",
          title: "Import Products",
          breadcrumb: "Import Products",
          icon: "fa fa-upload",
          isHome: false,
          hasSearchbar: true
        }
      },
      {
        path: "import-product-details",
        canActivateChild: [AuthHomeGuard],
        component: ImportProductDetailsComponent,
        data: {
          name: "import-product-details",
          class: "o-page",
          title: "Import Product Details",
          breadcrumb: "Import Product Details",
          icon: "fa fa-upload",
          isHome: false,
          hasSearchbar: true
        }
      },
      {
        path: "products-details",
        canActivateChild: [AuthHomeGuard],
        component: ProductDetailsComponent,
        data: {
          name: "product-details",
          class: "o-page",
          title: "Products Details",
          breadcrumb: "Product Details",
          icon: "fa fa-shopping-cart",
          isHome: false,
          hasSearchbar: false
        }
      },
      {
        path: "users",
        canActivateChild: [AuthHomeGuard],
        component: UsersComponent,
        data: {
          name: "users",
          class: "o-page",
          title: "Users",
          hasSearchbar: true
        }
      },
      {
        path: "users-detail",
        canActivateChild: [AuthHomeGuard],
        component: UserDetailsComponent,
        data: {
          name: "users-details",
          class: "o-page",
          title: "User Details",
          hasSearchbar: false
        }
      },
      {
        path: "social-links",
        canActivateChild: [AuthHomeGuard],
        component: SocialLinksComponent,
        data: {
          name: "social-links",
          class: "o-page",
          title: "Social Links",
          hasSearchbar: false
        }
      }
    ]
  }
];
