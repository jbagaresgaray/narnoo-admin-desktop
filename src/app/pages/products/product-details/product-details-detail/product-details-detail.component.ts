import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import * as _ from "lodash";

import { ProductsService } from "../../../../services/products.service";

declare let $: any;
declare let jQuery: any;

@Component({
  selector: "app-product-details-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-details-detail.component.html",
  styleUrls: ["./product-details-detail.component.css"]
})
export class ProductDetailsDetailComponent implements OnInit {
  token: string;
  productId: string;

  product: any = {};
  textEditorConfig: any = {};

  isSaving: boolean;
  submitted: boolean;
  showContent: boolean;
  isProductInformationSaving: boolean;

  timeForm: FormGroup;
  termsForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    public coolDialogs: NgxCoolDialogsService,
    public products: ProductsService
  ) {}

  ngOnInit() {
    this.productId = this.route.snapshot.queryParamMap.get("productId");
    this.token = localStorage.getItem("admin.bus.token");

    this.textEditorConfig = {
      toolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ align: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ size: ["small", false, "large", "huge"] }],
        [{ font: [] }]
      ]
    };
    this.isProductInformationSaving = false;

    this.timeForm = this.formBuilder.group({
      productId: ["", Validators.required],
      startTime: ["", Validators.required],
      endTime: ["", Validators.required],
      duration: ["", Validators.required],
      settingsBookable: ["", Validators.required],
      settingsLibrary: ["", Validators.required]
    });

    this.termsForm = this.formBuilder.group({
      productId: ["", Validators.required],
      termsText: ["", Validators.required],
      settingsBookable: ["", Validators.required],
      settingsLibrary: ["", Validators.required]
    });
    this.initData();
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  get fTime(): any {
    return this.timeForm.controls;
  }

  get fTerms(): any {
    return this.termsForm.controls;
  }

  private toggleTimeFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.timeForm.controls).forEach(controlName => {
      this.timeForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private toggleTermsFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.termsForm.controls).forEach(controlName => {
      this.termsForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private initData() {
    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;
        if (this.product.additionalInformation) {
          this.product.startTime = this.product.additionalInformation.startTime;
          this.product.endTime = this.product.additionalInformation.endTime;
          this.product.duration = this.product.additionalInformation.operatingHours;
          this.product.transportText = this.product.additionalInformation.transfer;
          this.product.purchasesText = this.product.additionalInformation.purchases;
          this.product.packingText = this.product.additionalInformation.packing;
          this.product.healthText = this.product.additionalInformation.fitness;
          this.product.childrenText = this.product.additionalInformation.child;
          this.product.additionalText = this.product.additionalInformation.additional;
          this.product.termsText = this.product.additionalInformation.terms;
        }
        this.product.settingsBookable = this.product.bookable;
        this.product.settingsLibrary = this.product.access;
      }
      this.showContent = true;
      this.timeForm.patchValue(this.product);
      this.termsForm.patchValue(this.product);
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = false;
    this.products
      .product_details(this.token, this.productId)
      .then(productResponse, errorResponse)
      .then(() => {
        setTimeout(() => {
          const options = {
            step: 15,
            timeFormat: "g:i A"
          };
          $(document).ready(function() {
            $("#operational-start-time").timepicker(options);
            $("#operational-end-time").timepicker(options);
          });
        }, 2000);
      });
  }

  saveProductChanges(action) {
    let confirmMsg: string;

    if (action === "time") {
      this.submitted = true;
      this.toggleTimeFormState();

      // $("#trainingSchedFrom").datepicker("setDate", this.product.startTime);

      this.timeForm.patchValue({
        productId: this.product.productId,
        settingsBookable: this.product.bookable,
        settingsLibrary: this.product.access,
        startTime: $("#operational-start-time").val(),
        endTime: $("#operational-end-time").val()
      });
      console.log("this.timeForm: ", this.timeForm.value);

      if (this.timeForm.invalid) {
        this.isSaving = false;
        this.validateAllFormFields(this.timeForm);
        this.toggleTimeFormState();
        return;
      }
    } else if (action === "terms") {
      this.submitted = true;
      this.toggleTermsFormState();

      this.termsForm.patchValue({
        productId: this.product.productId,
        settingsBookable: this.product.settingsBookable,
        settingsLibrary: this.product.settingsLibrary
      });

      if (this.termsForm.invalid) {
        this.isSaving = false;
        this.validateAllFormFields(this.termsForm);
        this.toggleTermsFormState();
        return;
      }
    }

    const callbackSave = () => {
      this.isProductInformationSaving = false;
    };

    const callbackConfirm = () => {
      confirmMsg = "Are you sure to save product information?";
    };

    const callbackToast = () => {
      this.isProductInformationSaving = true;
      this.toastr.info("Saving Product Information...", "INFO");
    };

    const saveProductChanges = () => {
      callbackConfirm();

      this.coolDialogs.confirm(confirmMsg).subscribe(res => {
        if (res) {
          callbackToast();

          let promise: any;
          this.isSaving = true;

          if (action === "time") {
            promise = this.products.product_edit(
              this.token,
              this.timeForm.value
            );
          } else if (action === "terms") {
            promise = this.products.product_edit(
              this.token,
              this.termsForm.value
            );
          } else {
            promise = this.products.product_edit(this.token, this.product);
          }

          promise.then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                this.isSaving = false;
                this.submitted = false;
                setTimeout(() => {
                  this.initData();
                }, 300);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
                this.isSaving = false;
                this.submitted = false;
              }

              callbackSave();
            },
            error => {
              console.log("error: ", error);
              this.isSaving = false;
              this.submitted = false;
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }

              callbackSave();
            }
          );
        }
      });
    };

    saveProductChanges();
  }
}
