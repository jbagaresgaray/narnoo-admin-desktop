import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { ProductsService } from "../../../../services/products.service";
import { DatapickerComponent } from "../../../datapicker/datapicker.component";

declare let $: any;

@Component({
  selector: "app-product-details-gallery",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-details-gallery.component.html",
  styleUrls: ["./product-details-gallery.component.css"]
})
export class ProductDetailsGalleryComponent implements OnInit {
  product: any = {};
  showContent: boolean;
  fakeArr: any[] = [];
  token: string;
  productId: string;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private changeDetection: ChangeDetectorRef,
    private modalService: BsModalService,
    private route: ActivatedRoute,
    private products: ProductsService
  ) {}

  ngOnInit() {
    for (let index = 0; index < 24; index++) {
      this.fakeArr.push(index);
    }
    this.productId = this.route.snapshot.queryParamMap.get("productId");
    this.token = localStorage.getItem("admin.bus.token");

    this.initData();
  }

  private initData() {
    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;
      }
      this.showContent = true;
      console.log("product: ", this.product);
      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
          image.src = previewImage.data("image");
        });
      }, 600);
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = false;
    this.products
      .product_details(this.token, this.productId)
      .then(productResponse, errorResponse);
  }

  addProductImages() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "gallery",
      title: "Add Product Image",
      params: this.product
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(DatapickerComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
