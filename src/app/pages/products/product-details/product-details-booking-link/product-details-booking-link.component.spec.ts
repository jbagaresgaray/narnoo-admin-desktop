import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDetailsBookingLinkComponent } from './product-details-booking-link.component';

describe('ProductDetailsBookingLinkComponent', () => {
  let component: ProductDetailsBookingLinkComponent;
  let fixture: ComponentFixture<ProductDetailsBookingLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDetailsBookingLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailsBookingLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
