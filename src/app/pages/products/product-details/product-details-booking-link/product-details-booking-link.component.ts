import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { ProductsService } from "src/app/services/products.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-product-details-booking-link",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-details-booking-link.component.html",
  styleUrls: ["./product-details-booking-link.component.css"]
})
export class ProductDetailsBookingLinkComponent implements OnInit {
  product: any = {};
  isBookingSaving: boolean;
  isSaving: boolean;
  submitted: boolean;
  showContent: boolean;

  productId: string;
  token: string;

  constructor(
    private route: ActivatedRoute,
    public toastr: ToastrService,
    public coolDialogs: NgxCoolDialogsService,
    private products: ProductsService
  ) {}

  ngOnInit() {
    this.isBookingSaving = false;

    this.token = localStorage.getItem("admin.bus.token");
    this.productId = this.route.snapshot.queryParamMap.get("productId");
    this.initData();
  }

  private initData() {
    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;
        this.product.bookingLink = this.product.directBooking;
        this.product.settingsBookable = this.product.bookable;
        this.product.settingsLibrary = this.product.access;
      }
      this.showContent = true;
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = false;
    this.products
      .product_details(this.token, this.productId)
      .then(productResponse, errorResponse);
  }

  saveProductChanges() {
    let confirmMsg: string;

    const callbackSave = () => {
      this.isBookingSaving = false;
    };

    const callbackConfirm = () => {
      confirmMsg = "Are you sure to save product booking link?";
    };

    const callbackToast = () => {
      this.isBookingSaving = true;
      this.toastr.info("Saving Booking link...", "INFO");
    };

    const saveProductChanges = () => {
      callbackConfirm();

      this.coolDialogs.confirm(confirmMsg).subscribe(res => {
        if (res) {
          callbackToast();
          this.product.bookingLink = this.product.directBooking;
          this.products.product_edit(this.token, this.product).then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.initData();
                }, 300);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }

              callbackSave();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }

              callbackSave();
            }
          );
        }
      });
    };

    saveProductChanges();
  }
}
