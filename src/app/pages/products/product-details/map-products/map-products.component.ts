import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { ByobService } from "../../../../services/byob.service";
import { ProductsService } from "../../../../services/products.service";

@Component({
  selector: "app-map-products",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./map-products.component.html",
  styleUrls: ["./map-products.component.css"]
})
export class MapProductsComponent implements OnInit {
  product: any = {};
  business: any = {};
  platformsArr: any[] = [];
  productMapping: any[] = [];

  @Output() _saveChanges = new EventEmitter<any>();

  token: string;
  productId: string;

  isProductMappingSaving: boolean;
  isSaving: boolean;
  submitted: boolean;
  showContent: boolean;

  constructor(
    public toastr: ToastrService,
    public coolDialogs: NgxCoolDialogsService,
    private route: ActivatedRoute,
    private products: ProductsService,
    private byob: ByobService
  ) {}

  ngOnInit() {
    console.log("this.productMapping: ", this.productMapping);
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    this.productId = this.route.snapshot.queryParamMap.get("productId");
    this.isProductMappingSaving = false;

    this.initData().then(() => {
      this.byob.admin_reservation_platforms(this.token).then(
        (data: any) => {
          if (data && data.success) {
            this.platformsArr = data.data.response;
            console.log("admin_reservation_platforms: ", this.platformsArr);
          }
          this.showContent = true;
        },
        error => {
          console.log("error: ", error);
        }
      );

      this.byob.admin_product_platforms(this.token, this.productId).then(
        (resp: any) => {
          if (resp && resp.success) {
            this.productMapping = resp.data;
            console.log("admin_product_platforms: ", this.productMapping);
          }
        },
        error => {
          console.log("admin_product_platforms ERROR: ", error);
        }
      );
    });
  }

  private async initData() {
    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;
        this.product.bookingLink = this.product.directBooking;
        this.product.settingsBookable = this.product.bookable;
        this.product.settingsLibrary = this.product.access;
      }
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = false;
    await this.products
      .product_details(this.token, this.productId)
      .then(productResponse, errorResponse);
  }

  saveProductChanges() {
    let confirmMsg: string;

    const callbackSave = () => {
      this.isProductMappingSaving = false;
    };

    const callbackConfirm = () => {
      confirmMsg = "Map product to reservation platform?";
    };

    const callbackToast = () => {
      this.isProductMappingSaving = true;
      this.toastr.info("Mapping product to reservation platform...", "INFO");
    };

    const saveProductChanges = () => {
      callbackConfirm();

      this.coolDialogs.confirm(confirmMsg).subscribe(res => {
        if (res) {
          callbackToast();

          this.byob
            .admin_mapping_product(this.token, {
              operator: this.business.id,
              product: this.product.productId,
              platform: this.product.platform,
              platformId: this.product.platformId
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("success: ", data);
                  this.toastr.success(
                    "Product mapping successfully!",
                    "SUCCESS"
                  );
                  setTimeout(() => {
                    this.initData();
                  }, 300);
                } else if (data && !data.success) {
                  this.toastr.warning(data.message, "WARNING");
                }
                callbackSave();
              },
              error => {
                console.log("error: ", error);
                if (error && !error.success) {
                  this.toastr.error(error.error, "ERROR");
                }
                callbackSave();
              }
            );
        }
      });
    };

    saveProductChanges();
  }
}
