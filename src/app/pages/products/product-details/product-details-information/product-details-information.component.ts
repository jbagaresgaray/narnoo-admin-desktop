import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { Router, ActivatedRoute } from "@angular/router";

import { ProductsService } from "../../../../services/products.service";

@Component({
  selector: "app-product-details-information",
  templateUrl: "./product-details-information.component.html",
  styleUrls: ["./product-details-information.component.css"]
})
export class ProductDetailsInformationComponent implements OnInit {
  features: any = {};
  product: any = {};
  business: any = {};
  textEditorConfig: any = {};

  newSummaryArr: any[] = [];
  newDescriptionArr: any[] = [];

  isSaving: boolean;
  submitted: boolean;
  showContent: boolean;

  isProductNameSaving: boolean;
  isProductDescriptionSaving: boolean;
  isProductInformationSaving: boolean;
  isProductFeaturesSaving: boolean;
  isProductSummarySaving: boolean;
  isProductKeywordsSaving: boolean;

  token: string;
  productId: string;

  entryForm: FormGroup;
  priceForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private products: ProductsService,
    public coolDialogs: NgxCoolDialogsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.product.keywordsArr = [];
    this.product.settingsLibrary = false;
    this.product.settingsBookable = false;

    this.isProductNameSaving = false;
    this.isProductSummarySaving = false;
    this.isProductDescriptionSaving = false;
    this.isProductFeaturesSaving = false;
    this.isProductKeywordsSaving = false;

    this.textEditorConfig = {
      toolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ align: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ size: ["small", false, "large", "huge"] }],
        [{ font: [] }]
      ]
    };

    this.submitted = false;

    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    this.productId = this.route.snapshot.queryParamMap.get("productId");
    console.log("this.productId: ", this.productId);

    this.entryForm = this.formBuilder.group({
      productId: ["", Validators.required],
      title: ["", Validators.required]
    });

    this.priceForm = this.formBuilder.group({
      productId: ["", Validators.required],
      price: ["", Validators.required],
      settingsLibrary: [""],
      settingsBookable: [""]
    });

    if (this.product) {
      this.entryForm.patchValue(this.product);
      this.priceForm.patchValue(this.product);
    }

    this.initData();
  }

  private initData() {
    this.newDescriptionArr = [];
    this.newSummaryArr = [];

    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;

        this.product.settingsBookable = this.product.bookable;
        this.product.settingsLibrary = this.product.access;

        this.product.price = this.product.avgPrice;
        if (this.product.keywords) {
          this.product.keywordsArr = _.map(
            this.product.keywords.split(","),
            (row: any) => {
              return {
                display: row,
                value: row
              };
            }
          );
        } else {
          this.product.keywordsArr = [];
        }

        if (this.product.description) {
          const descriptionArr = this.product.description.description;
          const summaryArr = this.product.description.summary;

          if (!_.isEmpty(descriptionArr)) {
            _.each(descriptionArr, (row: any) => {
              const arr: any = Object.keys(row).map(key => ({
                key,
                value: row[key],
                expanded: false,
                action: "desc",
                mode: "desc_" + key
              }));
              this.newDescriptionArr.push(arr[0]);
            });
          } else {
            this.newDescriptionArr.push(
              {
                key: "english",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "japanese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "chinese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "korean",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              },
              {
                key: "german",
                value: {
                  text: ""
                },
                expanded: false,
                action: "desc",
                mode: "desc_english"
              }
            );
          }

          if (!_.isEmpty(summaryArr)) {
            _.each(summaryArr, (row: any) => {
              const arr: any = Object.keys(row).map(key => ({
                key,
                value: row[key],
                expanded: false,
                action: "sum",
                mode: "sum_" + key
              }));
              this.newSummaryArr.push(arr[0]);
            });
          } else {
            this.newSummaryArr.push(
              {
                key: "english",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "japanese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "chinese",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "korean",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              },
              {
                key: "german",
                value: {
                  text: ""
                },
                expanded: false,
                action: "sum",
                mode: "sum_english"
              }
            );
          }
        } else {
          this.newDescriptionArr.push(
            {
              key: "english",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "japanese",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "chinese",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "korean",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            },
            {
              key: "german",
              value: {
                text: ""
              },
              expanded: false,
              action: "desc",
              mode: "desc_english"
            }
          );

          this.newSummaryArr.push(
            {
              key: "english",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "japanese",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "chinese",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "korean",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            },
            {
              key: "german",
              value: {
                text: ""
              },
              expanded: false,
              action: "sum",
              mode: "sum_english"
            }
          );
        }

        this.features = this.product.features;
        if (this.features) {
          // ==================================================================
          // ==================================================================
          // ==================================================================
          this.features.disabledAccess =
            this.features.disabledAccess === "1" ? true : false;
          this.features.giftShop =
            this.features.giftShop === "1" ? true : false;
          this.features.storageLockers =
            this.features.storageLockers === "1" ? true : false;
          this.features.visitorInformation =
            this.features.visitorInformation === "1" ? true : false;
          this.features.dinnerIncluded =
            this.features.dinnerIncluded === "1" ? true : false;
          this.features.internetAccess =
            this.features.internetAccess === "1" ? true : false;
          this.features.media = this.features.media === "1" ? true : false;
          this.features.functionHire =
            this.features.functionHire === "1" ? true : false;
          this.features.hotelPickupService =
            this.features.hotelPickupService === "1" ? true : false;
          this.features.mealProvided =
            this.features.mealProvided === "1" ? true : false;
          this.features.guidedTours =
            this.features.guidedTours === "1" ? true : false;
          this.features.weddingServices =
            this.features.weddingServices === "1" ? true : false;
          this.features.breakfestIncluded =
            this.features.breakfestIncluded === "1" ? true : false;
          this.features.cafe = this.features.cafe === "1" ? true : false;
          this.features.languageTranslation =
            this.features.languageTranslation === "1" ? true : false;
          this.features.conferenceServices =
            this.features.conferenceServices === "1" ? true : false;
          this.features.lunchIncluded =
            this.features.lunchIncluded === "1" ? true : false;
          // ==================================================================
          // ==================================================================
          // ==================================================================

          this.features["24HourDesk"] =
            this.features["24HourDesk"] === "1" ? true : false;
          this.features.bar = this.features.bar === "1" ? true : false;
          this.features.conciergeService =
            this.features.conciergeService === "1" ? true : false;
          this.features.conferenceServices =
            this.features.conferenceServices === "1" ? true : false;
          this.features.expressCheckIn =
            this.features.expressCheckIn === "1" ? true : false;
          this.features.expressCheckOut =
            this.features.expressCheckOut === "1" ? true : false;
          this.features.fitnessCentre =
            this.features.fitnessCentre === "1" ? true : false;
          this.features.kitchenFacilities =
            this.features.kitchenFacilities === "1" ? true : false;
          this.features.laundryFacilities =
            this.features.laundryFacilities === "1" ? true : false;
          this.features.meetingFacilities =
            this.features.meetingFacilities === "1" ? true : false;
          this.features.nonSmoking =
            this.features.nonSmoking === "1" ? true : false;
          this.features.parkingAccess =
            this.features.parkingAccess === "1" ? true : false;
          this.features.restaurant =
            this.features.restaurant === "1" ? true : false;
          this.features.roomService =
            this.features.roomService === "1" ? true : false;
          this.features.safeDeposit =
            this.features.safeDeposit === "1" ? true : false;
          this.features.swimmingPool =
            this.features.swimmingPool === "1" ? true : false;
          this.features.tourBookingService =
            this.features.tourBookingService === "1" ? true : false;
          this.features.wifiAccess =
            this.features.wifiAccess === "1" ? true : false;
        } else {
          this.features = {};
        }
      }
      this.showContent = true;

      console.log("product detail: ", this.product);

      this.entryForm.patchValue(this.product);
      this.priceForm.patchValue(this.product);
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = false;
    this.products
      .product_details(this.token, this.productId)
      .then(productResponse, errorResponse);
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  saveProductNameChanges() {
    this.submitted = true;
    this.toggleFormState();

    if (this.entryForm.invalid) {
      this.toggleFormState();
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to save Product name/title?")
      .subscribe(res => {
        if (res) {
          this.isProductNameSaving = false;
          this.toastr.info("Saving product summary...", "INFO");
          this.products
            .product_edit_title(this.token, {
              productId: this.product.productId,
              title: this.entryForm.value.title
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("success: ", data);
                  this.toastr.success(data.data, "SUCCESS");
                  setTimeout(() => {
                    this.initData();
                  }, 300);
                } else if (data && !data.success) {
                  this.toastr.warning(data.message, "WARNING");
                }
                this.isProductNameSaving = false;
              },
              error => {
                console.log("error: ", error);
                this.isProductNameSaving = false;
                if (error && !error.success) {
                  this.toastr.error(error.error, "ERROR");
                }
              }
            );
        }
      });
  }

  saveProductFeatures() {
    let featuresObj: any = {};
    console.log("this.product.category: ", this.product.category);

    if (this.product.category === "service") {
      featuresObj = {
        productId: this.product.productId,
        disabilityAccess: this.features.disabledAccess,
        internet: this.features.internetAccess,
        foodIncluded: this.features.mealProvided,
        cafe: this.features.cafe,
        giftShop: this.features.giftShop,
        mediaService: this.features.media,
        guidedTours: this.features.guidedTours,
        translation: this.features.languageTranslation,
        lockers: this.features.storageLockers,
        functions: this.features.functionHire,
        weddings: this.features.weddingServices,
        conference: this.features.conferenceServices,
        vistiorInformation: this.features.visitorInformation,
        pickUp: this.features.hotelPickupService,
        breakfast: this.features.breakfestIncluded,
        lunch: this.features.lunchIncluded,
        dinner: this.features.dinnerIncluded
      };
    } else if (this.product.category === "accommodation") {
      featuresObj = {
        expressCheckIn: this.features.expressCheckIn,
        expressCheckOut: this.features.expressCheckOut,
        desk24hr: this.features["24HourDesk"],
        concierge: this.features.conciergeService,
        tourDesk: this.features.tourBookingService,
        fitness: this.features.fitnessCentre,
        parking: this.features.parkingAccess,
        meetingFacilities: this.features.meetingFacilities,
        laundry: this.features.laundryFacilities,
        restaurant: this.features.restaurant,
        roomService: this.features.roomService,
        kitchen: this.features.kitchenFacilities,
        internet: this.features.internetAccess,
        wifi: this.features.wifiAccess,
        nonsmoking: this.features.nonSmoking,
        disabilityAccess: this.features.disabledAccess,
        safe: this.features.safeDeposit,
        weddings: this.features.weddingServices,
        conference: this.features.conferenceServices,
        bar: this.features.bar,
        swimming: this.features.swimmingPool
      };
    } else {
      featuresObj = {
        productId: this.product.productId,
        expressCheckIn: this.features.expressCheckIn,
        expressCheckOut: this.features.expressCheckOut,
        desk24hr: this.features["24HourDesk"],
        concierge: this.features.conciergeService,
        tourDesk: this.features.tourBookingService,
        fitness: this.features.fitnessCentre,
        parking: this.features.parkingAccess,
        meetingFacilities: this.features.meetingFacilities,
        laundry: this.features.laundryFacilities,
        restaurant: this.features.restaurant,
        roomService: this.features.roomService,
        kitchen: this.features.kitchenFacilities,
        internet: this.features.internetAccess,
        wifi: this.features.wifiAccess,
        nonsmoking: this.features.nonSmoking,
        disabilityAccess: this.features.disabledAccess,
        safe: this.features.safeDeposit,
        weddings: this.features.weddingServices,
        conference: this.features.conferenceServices,
        bar: this.features.bar,
        swimming: this.features.swimmingPool,
        foodIncluded: this.features.mealProvided,
        cafe: this.features.cafe,
        giftShop: this.features.giftShop,
        mediaService: this.features.media,
        guidedTours: this.features.guidedTours,
        translation: this.features.languageTranslation,
        lockers: this.features.storageLockers,
        functions: this.features.functionHire,
        vistiorInformation: this.features.visitorInformation,
        pickUp: this.features.hotelPickupService,
        breakfast: this.features.breakfestIncluded,
        lunch: this.features.lunchIncluded,
        dinner: this.features.dinnerIncluded
      };
    }

    console.log("featuresObj: ", featuresObj);
    this.coolDialogs
      .confirm("Are you sure to save product features?")
      .subscribe(res => {
        if (res) {
          this.isProductFeaturesSaving = true;
          this.products.product_edit_features(this.token, featuresObj).then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.initData();
                }, 300);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
              this.isProductFeaturesSaving = false;
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }
              this.isProductFeaturesSaving = false;
            }
          );
        }
      });
  }

  saveProductKeywords() {
    if (!_.isEmpty(this.product.keywordsArr)) {
      this.product.keywords = _.map(this.product.keywordsArr, (row: any) => {
        return row.value;
      }).join(",");
    }

    this.coolDialogs
      .confirm("Are you sure to save product keywords?")
      .subscribe(res => {
        if (res) {
          this.isProductKeywordsSaving = true;
          this.products
            .product_edit(this.token, {
              productId: this.product.productId,
              keywords: this.product.keywords,
              settingsLibrary: this.product.settingsLibrary,
              settingsBookable: this.product.settingsBookable
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("success: ", data);
                  this.toastr.success(data.data, "SUCCESS");
                  setTimeout(() => {
                    this.initData();
                  }, 300);
                } else if (data && !data.success) {
                  this.toastr.warning(data.message, "WARNING");
                }
                this.isProductKeywordsSaving = false;
              },
              error => {
                console.log("error: ", error);
                if (error && !error.success) {
                  this.toastr.error(error.error, "ERROR");
                }
                this.isProductKeywordsSaving = false;
              }
            );
        }
      });
  }

  saveProductChanges(action, mode?: any, product?: any) {
    const _action = action === "sum" ? "Summary" : "Description";
    let confirmMsg: string;

    if (product && _.isEmpty(product.value.text)) {
      this.toastr.warning("Please fill up " + _action + " fields");
      return;
    }

    if (action === "information") {
      this.submitted = true;
      this.toggleFormState();

      if (this.priceForm.invalid) {
        this.toggleFormState();
        return;
      }
    }

    const callbackSave = () => {
      if (action === "information") {
        this.isProductInformationSaving = false;
      } else if (action === "desc") {
        this.isProductDescriptionSaving = false;
      } else if (action === "sum") {
        this.isProductSummarySaving = false;
      }
    };

    const callbackConfirm = () => {
      if (action === "information") {
        confirmMsg = "Are you sure to save product information?";
      } else if (action === "desc") {
        const _mode = _.split(mode, "_");
        confirmMsg =
          "Are you sure to save " +
          _.startCase(_mode[1]) +
          " product description?";
        if (product) {
          this.product["description" + _.startCase(_mode[1])] =
            product.value.text;
        }
      } else if (action === "sum") {
        const _mode = _.split(mode, "_");
        confirmMsg =
          "Are you sure to save " + _.startCase(_mode[1]) + " product summary?";
        if (product) {
          this.product["summary" + _.startCase(_mode[1])] = product.value.text;
        }
      }
    };

    const callbackToast = () => {
      if (action === "information") {
        this.isProductInformationSaving = true;
        this.toastr.info("Saving Product Information...", "INFO");
      } else if (action === "desc") {
        this.isProductDescriptionSaving = true;
        const _mode = _.split(mode, "_");
        this.toastr.info(
          "Saving " + _.startCase(_mode[1]) + " product description...",
          "INFO"
        );
      } else if (action === "sum") {
        this.isProductDescriptionSaving = true;
        const _mode = _.split(mode, "_");
        this.toastr.info(
          "Saving " + _.startCase(_mode[1]) + " product summary...",
          "INFO"
        );
      }
    };

    const saveProductChanges = () => {
      callbackConfirm();

      this.coolDialogs.confirm(confirmMsg).subscribe(res => {
        if (res) {
          callbackToast();
          let sSQL = null;
          if (action === "information") {
            this.priceForm.patchValue({
              settingsLibrary: this.product.settingsLibrary,
              settingsBookable: this.product.settingsBookable
            });
            sSQL = this.products.product_edit(this.token, this.priceForm.value);
          } else {
            sSQL = this.products.product_edit(this.token, this.product);
          }

          sSQL.then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.initData();
                }, 300);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }

              callbackSave();
            },
            error => {
              console.log("error: ", error);
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }

              callbackSave();
            }
          );
        }
      });
    };

    saveProductChanges();
  }

  saveChanges() {}
}
