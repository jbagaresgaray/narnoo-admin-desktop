import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  NgZone
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import { combineLatest, Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Location } from "@angular/common";

import { DatapickerComponent } from "../../datapicker/datapicker.component";

import { ProductsService } from "../../../services/products.service";
import { UtilitiesService } from "../../../services/utilities.service";

declare let $: any;

@Component({
  selector: "app-product-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-details.component.html",
  styleUrls: ["./product-details.component.css"]
})
export class ProductDetailsComponent implements OnInit {
  token: string;
  action: string;

  connect_params: any = {};
  business: any = {};
  productId: string;
  product: any = {};
  features: any = {};

  fakeArr: any[] = [];

  pageProduct: number;
  perPage: number;

  imageAction: string;

  showPreview: boolean;
  showContent: boolean;

  isInfo: boolean;
  isDetails: boolean;
  isGallery: boolean;
  isBooking: boolean;
  isMapping: boolean;

  showBookingSettings: boolean;
  showGalleryContent: boolean;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    public zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private products: ProductsService,
    private utilities: UtilitiesService,
    public location: Location
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.pageProduct = 0;
    this.perPage = 10;

    this.imageAction = "overview";

    this.showPreview = false;
    this.showContent = true;

    this.isInfo = true;
    this.isDetails = false;
    this.isGallery = false;
    this.isBooking = false;
    this.isMapping = false;

    this.showBookingSettings = true;
    this.showGalleryContent = false;

    this.productId = this.route.snapshot.queryParamMap.get("productId");
    console.log("this.productId: ", this.productId);
    this.initData();
  }

  private initData() {
    const productResponse = (data: any) => {
      if (data && data.success) {
        this.product = data.data;
      }
      this.showContent = true;
    };

    const errorResponse = error => {
      this.showContent = false;
      console.log("product detail ERROR: ", error);
    };

    this.showContent = false;
    this.products
      .product_details(this.token, this.productId)
      .then(productResponse, errorResponse);
  }

  back() {
    this.location.back();
  }

  refresh() {
    this.initData();
  }

  showTab(tab) {
    if (tab === "info") {
      this.isInfo = true;
      this.isDetails = false;
      this.isGallery = false;
      this.isBooking = false;
      this.isMapping = false;
    } else if (tab === "details") {
      this.isInfo = false;
      this.isDetails = true;
      this.isGallery = false;
      this.isBooking = false;
      this.isMapping = false;
    } else if (tab === "gallery") {
      this.isInfo = false;
      this.isDetails = false;
      this.isGallery = true;
      this.isBooking = false;
      this.isMapping = false;
    } else if (tab === "booking") {
      this.isInfo = false;
      this.isDetails = false;
      this.isGallery = false;
      this.isBooking = true;
      this.isMapping = false;
    } else if (tab === "mapping") {
      this.isInfo = false;
      this.isDetails = false;
      this.isGallery = false;
      this.isBooking = false;
      this.isMapping = true;
    }
  }

  setProductFeature(type) {
    let title: string;
    let action: string;

    if (type === "image") {
      title = "Set Feature Image";
      action = "feature_image";
    } else if (type === "video") {
      title = "Set Feature Video";
      action = "feature_video";
    } else if (type === "print") {
      title = "Set Feature Print Item";
      action = "feature_print";
    } else if (type === "logo") {
      title = "Set Feature Logo";
      action = "feature_logo";
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action,
      title: title,
      params: this.product
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(DatapickerComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteProduct() {
    const deleteProduct = () => {
      this.products
        .product_delete(this.token, {
          productId: this.product.productId
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.toastr.success(data.data, "SUCCESS");
              this.router.navigate(["/products"]);
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    };

    this.coolDialogs
      .confirm("Are you sure to delete this product?")
      .subscribe(res => {
        if (res) {
          deleteProduct();
        }
      });
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
