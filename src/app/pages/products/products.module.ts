import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  ModalModule,
  BsDropdownModule,
  PaginationModule,
  TabsModule
} from "ngx-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ClipboardModule } from "ngx-clipboard";
import { QuillModule } from "ngx-quill";
import { TagInputModule } from "ngx-chips";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { ProductsComponent } from "./products.component";
import { ProductEntryComponent } from "./product-entry/product-entry.component";
import { ProductBookingLinkComponent } from "./product-booking-link/product-booking-link.component";
import { ProductDetailsComponent } from "./product-details/product-details.component";
import { ProductDetailsInformationComponent } from "./product-details/product-details-information/product-details-information.component";
import { ProductDetailsDetailComponent } from "./product-details/product-details-detail/product-details-detail.component";
import { ProductDetailsGalleryComponent } from "./product-details/product-details-gallery/product-details-gallery.component";
import { ProductDetailsBookingLinkComponent } from "./product-details/product-details-booking-link/product-details-booking-link.component";
import { ImportProductComponent } from "./import-product/import-product.component";
import { ImportProductDetailsComponent } from "./import-product-details/import-product-details.component";
import { MapProductsComponent } from "./product-details/map-products/map-products.component";

@NgModule({
  declarations: [
    ProductsComponent,
    ProductEntryComponent,
    ProductBookingLinkComponent,
    ProductDetailsComponent,
    ProductDetailsInformationComponent,
    ProductDetailsDetailComponent,
    ProductDetailsGalleryComponent,
    ProductDetailsBookingLinkComponent,
    ImportProductComponent,
    ImportProductDetailsComponent,
    MapProductsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ModalModule,
    BsDropdownModule,
    PaginationModule,
    ComponentsModule,
    PipesModule,
    ClipboardModule,
    QuillModule,
    TabsModule,
    TagInputModule,
    ReactiveFormsModule
  ],
  entryComponents: [ProductEntryComponent, ProductBookingLinkComponent]
})
export class ProductsModule {}
