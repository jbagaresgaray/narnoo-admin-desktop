import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";

import { ProductsService } from "../../../services/products.service";
import { UtilitiesService } from "../../../services/utilities.service";

import { ProductEntryComponent } from "../../products/product-entry/product-entry.component";

@Component({
  selector: "app-import-product",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./import-product.component.html",
  styleUrls: ["./import-product.component.css"]
})
export class ImportProductComponent implements OnInit {
  token: string;
  action: string;
  connect_params: any = {};

  business: any = {};

  productInfo: any = {};
  productArr: any[] = [];
  productArrCopy: any[] = [];
  fakeArr: any[] = [];

  showContentProducts = false;
  showContentProductsErr = false;
  isGrid = true;
  productsContentErr: any = {};

  pageProduct = 1;
  totalPageProduct = 0;
  perPage = 20;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private products: ProductsService,
    private utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};

    for (let i = 0; i < 30; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.pageProduct = 1;
    this.initData();
  }

  private initData() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.productArr = [];
        this.productArrCopy = [];

        const products = data.data;
        this.productInfo = {
          description: data.description,
          platform: data.platform
        };
        this.totalPageProduct = parseInt(data.total, 0);
        for (let i = 0; i < _.size(products); i++) {
          products[i].selected = false;
          this.productArr.push(products[i]);
        }
        this.productArrCopy = _.cloneDeep(this.productArr);
      }
      console.log("products: ", this.productArr);
      this.showContentProducts = true;
    };

    const errorResponse = (error: any) => {
      this.showContentProducts = true;
      this.showContentProductsErr = true;
      if (error && !error.success) {
        this.productsContentErr = error;
      }
    };

    this.products
      .product_import_list(this.token)
      .then(successResponse, errorResponse);
  }

  createProduct() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "create"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  refresh() {
    this.pageProduct = 1;
    this.productArr = [];
    this.productArrCopy = [];
    this.initData();
  }

  pageChanged(event: any): void {
    console.log("Page changed to: " + event.page);
    console.log("Number items per page: " + event.itemsPerPage);
    this.pageProduct = event.page;
    this.initData();
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  gotoDetails(item) {
    const params: any = {
      queryParams: { platform: this.productInfo.platform, code: item.code },
      queryParamsHandling: "merge"
    };
    this.router.navigate(["/import-product-details"], params);
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
