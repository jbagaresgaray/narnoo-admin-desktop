import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportProductDetailsComponent } from './import-product-details.component';

describe('ImportProductDetailsComponent', () => {
  let component: ImportProductDetailsComponent;
  let fixture: ComponentFixture<ImportProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
