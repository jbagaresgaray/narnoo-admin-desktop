import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";

import { ProductsService } from "../../../services/products.service";
import { UtilitiesService } from "../../../services/utilities.service";

@Component({
  selector: "app-import-product-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./import-product-details.component.html",
  styleUrls: ["./import-product-details.component.css"]
})
export class ImportProductDetailsComponent implements OnInit {
  token: string;
  business: any = {};
  productInfo: any = {};

  platform: string;
  productCode: string;

  showContent: boolean = false;
  isSaving: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    public coolDialogs: NgxCoolDialogsService,
    private products: ProductsService,
    private utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  private initData() {
    this.showContent = false;
    this.products
      .product_import_details(this.token, {
        platform: this.platform,
        code: this.productCode
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.productInfo = data.data;
          }
          this.showContent = true;
        },
        error => {
          console.log("Error: ", error);
          this.showContent = true;
        }
      );
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      console.log("params: ", params); // {order: "popular"}
      if (params.platform && params.code) {
        this.platform = params.platform;
        this.productCode = params.code;
        this.initData();
      }
    });
  }

  importProduct() {
    const importProductAPI = () => {
      this.isSaving = true;
      this.products
        .product_import_data(this.token, {
          platform: this.platform,
          code: this.productCode
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.toastr.success(data.message, "SUCCESS");
              setTimeout(() => {
                this.router.navigate(["/import-products"]);
              },600);
            } else if (data && !data.success) {
              this.toastr.error(data.message, "WARNING");
            }
            this.isSaving = false;
          },
          (error: any) => {
            console.log("error: ", error);
            this.isSaving = false;
          }
        );
    };
    this.coolDialogs.confirm("Import this product?").subscribe(res => {
      if (res) {
        importProductAPI();
      }
    });
  }

  refresh() {
    this.initData();
  }
}
