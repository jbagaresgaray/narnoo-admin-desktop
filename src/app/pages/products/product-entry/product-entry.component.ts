import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { ProductsService } from "../../../services/products.service";

@Component({
  selector: "app-product-entry",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./product-entry.component.html",
  styleUrls: ["./product-entry.component.css"]
})
export class ProductEntryComponent implements OnInit {
  isSaving: boolean;
  submitted: boolean;

  business: any = {};
  product: any = {};
  token: string;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private productService: ProductsService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.isSaving = false;
    this.submitted = false;

    this.entryForm = this.formBuilder.group({
      title: ["", Validators.required]
    });
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  saveChanges() {
    this.submitted = true;

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      return;
    }

    this.coolDialog
      .confirm("Are you sure to save new product?")
      .subscribe(res => {
        if (res) {
          this.isSaving = true;
          this.toggleFormState();

          this.toastr.info("Saving...", "INFO");
          this.productService
            .product_create(this.token, {
              title: this.product.title
            })
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("success: ", data);
                  this.toastr.success(data.message, "SUCCESS");
                  this.modalService.setDismissReason("save");
                  this.bsModalRef.hide();
                } else if (data && !data.success) {
                  this.toastr.warning(data.message, "WARNING");
                }
                this.isSaving = false;
              },
              error => {
                console.log("error: ", error);
                this.isSaving = false;
                if (error && !error.success) {
                  this.toastr.error(error.error, "ERROR");
                }
              }
            );
        }
      });
  }
}
