import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { BroadcasterService } from "ng-broadcaster";

import { ProductsService } from "../../services/products.service";
import { UtilitiesService } from "../../services/utilities.service";

import { ProductEntryComponent } from "../products/product-entry/product-entry.component";
import { ProductBookingLinkComponent } from "../products/product-booking-link/product-booking-link.component";

@Component({
  selector: "app-products",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.css"]
})
export class ProductsComponent implements OnInit {
  token: string;
  action: string;
  connect_params: any = {};

  business: any = {};

  productArr: any[] = [];
  productArrCopy: any[] = [];
  fakeArr: any[] = [];

  showContentProducts = false;
  showContentProductsErr = false;
  isGrid = false;
  productsContentErr: any = {};

  pageProduct = 1;
  totalPageProduct = 0;
  perPage = 20;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public toastr: ToastrService,
    private changeDetection: ChangeDetectorRef,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private broadcaster: BroadcasterService,
    private products: ProductsService,
    private utilities: UtilitiesService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    // this.action = navParams.get('action');
    // this.connect_params = navParams.get('params');

    for (let i = 0; i < 30; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initData() {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.productArr = [];
        this.productArrCopy = [];

        const products = data.data.products;
        this.pageProduct = parseInt(data.data.currentPage, 0);
        this.totalPageProduct = parseInt(data.data.totalPages, 0);

        for (let i = 0; i < _.size(products); i++) {
          products[i].selected = false;
          this.productArr.push(products[i]);
        }
        this.productArrCopy = _.cloneDeep(this.productArr);
      }
      console.log("products: ", this.productArr);
      this.showContentProducts = true;
    };

    const errorResponse = (error: any) => {
      this.showContentProducts = true;
      this.showContentProductsErr = true;
      if (error && !error.success) {
        this.productsContentErr = error;
      }
    };

    this.showContentProducts = false;
    this.products
      .product_list(this.token, {
        page: this.pageProduct,
        total: this.perPage
      })
      .then(successResponse, errorResponse);
  }

  ngOnInit() {
    this.broadcaster.on<any>("search::product").subscribe(value => {
      console.log("search::product: ", value);
    });

    this.pageProduct = 1;
    this.initData();
  }

  refresh() {
    this.pageProduct = 1;
    this.productArr = [];
    this.productArrCopy = [];

    this.initData();
  }

  pageChanged(event: any): void {
    console.log("Page changed to: " + event.page);
    console.log("Number items per page: " + event.itemsPerPage);
    this.pageProduct = event.page;
    this.initData();
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  gotoDetails(item) {
    const params: any = {
      queryParams: { productId: item.productId },
      queryParamsHandling: "merge"
    };
    this.router.navigate(["/products-details"], params);
  }

  deleteProduct(item: any) {
    const deleteProduct = () => {
      this.products
        .product_delete(this.token, {
          productId: item.productId
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.productArr = [];
              this.productArrCopy = [];
              this.toastr.success(data.data, "SUCCESS");
              this.refresh();
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    };

    this.coolDialogs.confirm("Delete selected product?").subscribe(res => {
      if (res) {
        deleteProduct();
      }
    });
  }

  createProduct() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "create"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  bookingLink(item: any) {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      productId: item.productId
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      ProductBookingLinkComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
