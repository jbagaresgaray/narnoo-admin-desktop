import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { ProductsService } from "../../../services/products.service";

@Component({
  selector: "app-product-booking-link",
  templateUrl: "./product-booking-link.component.html",
  styleUrls: ["./product-booking-link.component.css"]
})
export class ProductBookingLinkComponent implements OnInit {
  isSaving: boolean;
  showContent: boolean;

  business: any = {};
  product: any = {};
  token: string;
  productId: string;

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private productService: ProductsService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.isSaving = false;
    this.initData();
  }

  private initData() {
    this.showContent = false;

    this.productService.product_details(this.token, this.productId).then(
      (data: any) => {
        if (data && data.success) {
          this.product = data.data;
        }
        console.log("this.product: ", this.product);
        this.showContent = true;
      },
      (error: any) => {
        this.showContent = false;
        console.log("Product Details Error: ", error);
      }
    );
  }

  saveChanges() {
    this.coolDialog
      .confirm("Are you sure to save booking link?")
      .subscribe(res => {
        if (res) {
          this.isSaving = true;
          this.toastr.info("Saving...", "INFO");
          this.product.settingsBookable = false;
          this.product.settingsLibrary = false;
          this.product.bookingLink = this.product.directBooking;
          this.productService.product_edit(this.token, this.product).then(
            (data: any) => {
              if (data && data.success) {
                console.log("success: ", data);
                this.toastr.success(data.data, "SUCCESS");
                this.modalService.setDismissReason("save");
                this.bsModalRef.hide();
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
              this.isSaving = false;
            },
            error => {
              console.log("error: ", error);
              this.isSaving = false;
              if (error && !error.success) {
                this.toastr.error(error.error, "ERROR");
              }
            }
          );
        }
      });
  }
}
