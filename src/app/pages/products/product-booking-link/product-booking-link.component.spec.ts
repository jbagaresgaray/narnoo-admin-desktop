import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductBookingLinkComponent } from './product-booking-link.component';

describe('ProductBookingLinkComponent', () => {
  let component: ProductBookingLinkComponent;
  let fixture: ComponentFixture<ProductBookingLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductBookingLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBookingLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
