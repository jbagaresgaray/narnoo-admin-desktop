import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import * as $ from "jquery";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  constructor(public router: Router) {}

  ngOnInit() {
    $("body").addClass("o-page");
    $("body").addClass("o-page--center");
  }

  login() {
    this.router.navigate(["/login"]);
  }

  registerUser() {
    this.router.navigate(["/register"]);
  }
}
