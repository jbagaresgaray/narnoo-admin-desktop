import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import * as $ from "jquery";

import { LoginService } from "../../services/login.service";
import { UsersService } from "../../services/users.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  users: any = {};
  isRound: boolean = false;
  showFooter: boolean = true;

  constructor(
    public router: Router,
    private toastr: ToastrService,
    public services: LoginService,
    public userServices: UsersService
  ) {}

  ngOnInit() {
    $("body").addClass("o-page");
    $("body").addClass("o-page--center");
  }

  loginApp() {
    this.services.authenticate(this.users).then(
      (data: any) => {
        if (data && data.success) {
          localStorage.setItem("app.adminData", JSON.stringify(data.userData));
          localStorage.setItem("app.admintoken", data.token);

          this.router.navigate(["/home"]);
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
      },
      error => {
        console.log("error: ", error);
        if (error) {
          this.toastr.error(error.message, "WARNING");
        }
      }
    );
  }

  forgotPassword() {
    this.router.navigate(["/forgot"]);
  }

  registerUser() {
    this.router.navigate(["/register"]);
  }
}
