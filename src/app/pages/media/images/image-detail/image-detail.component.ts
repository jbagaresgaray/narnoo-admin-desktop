import { Component, OnInit, NgZone, ViewEncapsulation } from "@angular/core";
import * as $ from "jquery";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { combineLatest, Subscription } from "rxjs";
import { Location } from "@angular/common";
import {
  map,
  take,
  delay,
  withLatestFrom,
  finalize,
  tap
} from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ElectronService } from "ngx-electron";
import { LoadingBarService } from "@ngx-loading-bar/core";

import { ImagesService } from "../../../../services/images.service";
import { AlbumsService } from "../../../../services/albums.service";
import { ProductsService } from "../../../../services/products.service";

import { ImageShareComponent } from "../../sharer/image-share.component";

@Component({
  selector: "app-image-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./image-detail.component.html",
  styleUrls: ["./image-detail.component.css"]
})
export class ImageDetailComponent implements OnInit {
  token: string;
  action: string;
  imageId: string;

  item: any = {};
  params: any = {};
  business: any = {};

  showDownload: boolean;

  showError: boolean = false;
  showLoading: boolean = true;

  isGeneral: boolean;
  isMarket: boolean;
  isEmbededInfo: boolean;

  disabledMarket: boolean = true;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public zone: NgZone,
    public toastr: ToastrService,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private images: ImagesService,
    private albums: AlbumsService,
    private products: ProductsService,
    private _electronService: ElectronService,
    private loadingBar: LoadingBarService,
    private location: Location
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.isGeneral = true;
    this.item.markets = {};

    this.route.queryParams.subscribe((params: any) => {
      console.log("params: ", params); // {order: "popular"}

      if (params.imageId) {
        this.showLoading = true;
        this.imageId = params.imageId;
        this.initData();
      }
    });
  }

  private initData() {
    console.log("initData");
    const successResponse = (data: any) => {
      if (data && data.success) {
        console.log("data: ", data);
        this.item = data.data;
        this.item.imageId = data.data.id;
        this.item.imageSize = parseFloat(this.item.imageSize);
        if (_.isEmpty(this.item.caption)) {
          this.item.caption = "No caption available";
        }

        if (this.item.markets) {
          this.item.markets.australia =
            this.item.markets.australia === "1" ? true : false;
          this.item.markets.france =
            this.item.markets.france === "1" ? true : false;
          this.item.markets.germany =
            this.item.markets.germany === "1" ? true : false;
          this.item.markets.greaterChina =
            this.item.markets.greaterChina === "1" ? true : false;
          this.item.markets.india =
            this.item.markets.india === "1" ? true : false;
          this.item.markets.japan =
            this.item.markets.japan === "1" ? true : false;
          this.item.markets.korea =
            this.item.markets.korea === "1" ? true : false;
          this.item.markets.newZealand =
            this.item.markets.newZealand === "1" ? true : false;
          this.item.markets.northAmerica =
            this.item.markets.northAmerica === "1" ? true : false;
          this.item.markets.singapore =
            this.item.markets.singapore === "1" ? true : false;
          this.item.markets.unitedKindom =
            this.item.markets.unitedKindom === "1" ? true : false;
        } else {
          this.item.markets = {};
        }
      }
      this.showError = false;
      this.showLoading = false;

      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");
          image.src = previewImage.data("image");
          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
        });
      }, 1000);
    };

    const errorResponse = error => {
      console.log("errorResponse: ", error);
      this.showError = true;
      this.showLoading = false;
    };

    this.images
      .image_detail(this.token, this.imageId)
      .then(successResponse, errorResponse);
  }

  back() {
    this.location.back();
  }

  downloadImage() {
    this.toastr.info("Downloading ...");
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.images.image_download(this.token, this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          const filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (this._electronService.isElectronApp) {
            download(data.data, filename);
            this.toastr.success("Image successfully downloaded", "SUCCESS");
          } else {
            download(data.data, filename);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  shareImage() {
    const initialState: any = {
      imageId: this.item.id,
      action: "image"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(ImageShareComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteImage() {
    this.coolDialogs
      .confirm("Are you sure to delete this image?")
      .subscribe(res => {
        if (res) {
          this.toastr.info("Deleting...", "INFO");
          this.images.image_delete(this.token, [this.item.id]).then(
            (data: any) => {
              if (data && data.success) {
                console.log("image_delete: ", data);

                this.toastr.success("Image successfully deleted!", "SUCCESS");
                setTimeout(() => {
                  this.router.navigate(["/media"]);
                }, 600);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  featureImage() {
    this.coolDialogs
      .confirm("Are you sure to feature this image?")
      .subscribe(res => {
        if (res) {
          this.toastr.info("Featuring...", "INFO");
          this.images.image_feature(this.token, this.item).then(
            (data: any) => {
              if (data && data.success) {
                console.log("image_delete: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.initData();
                }, 600);
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  showTab(action) {
    if (this.showLoading) {
      return;
    }

    if (action === "general") {
      this.zone.run(() => {
        this.isGeneral = true;
        this.isMarket = false;
        this.isEmbededInfo = false;
      });
    } else if (action === "market") {
      this.zone.run(() => {
        this.isGeneral = false;
        this.isMarket = true;
        this.isEmbededInfo = false;
      });
    } else if (action === "embed") {
      this.zone.run(() => {
        this.isGeneral = false;
        this.isMarket = false;
        this.isEmbededInfo = true;
      });
    }

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 300);
  }

  copied(event) {
    if (event.isSuccess) {
      this.toastr.info("Copied!");
    }
  }
}
