import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  NgZone,
  ViewChild
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ContextMenuComponent, ContextMenuService } from "ngx-contextmenu";
import { combineLatest, Subscription } from "rxjs";
import * as xml2js from "xml2js";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { LoadingBarService } from "@ngx-loading-bar/core";
import { ElectronService } from "ngx-electron";
import { BroadcasterService } from "ng-broadcaster";
import {
  DropzoneComponent,
  DropzoneDirective,
  DropzoneConfigInterface
} from "ngx-dropzone-wrapper";

import * as _ from "lodash";
import * as $ from "jquery";

import { ImagesService } from "../../../services/images.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { UploadService } from "../../../services/upload.service";

import { environment } from "../../../../environments/environment";

import { UploadComponent } from "../../upload/upload.component";
import { ImageDetailComponent } from "./image-detail/image-detail.component";

@Component({
  selector: "app-media-images",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./images.component.html",
  styleUrls: ["./images.component.css"]
})
export class ImagesComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesArr: any[] = [];
  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  accessKey: string;
  action_uri: string;
  file_name: string;
  policy: string;
  signature: string;
  file_key: string;
  contentType: string;
  successActionStatus: string;
  acl: string;

  showPreview = false;
  showUpload = false;
  showDragDropUpload = false;

  showContentImage = false;
  showContentImageErr = false;
  showLoading = false;

  imageContentErr: any = {};
  pageImage = 0;
  totalPageImage = 0;
  fileAdded = 0;

  perPage = 21;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  @ViewChild(ContextMenuComponent)
  public basicMenu: ContextMenuComponent;

  @ViewChild(DropzoneDirective)
  componentRef?: DropzoneDirective;

  public config: DropzoneConfigInterface = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private zone: NgZone,
    public images: ImagesService,
    private modalService: BsModalService,
    public toastr: ToastrService,
    private contextMenuService: ContextMenuService,
    private coolDialogs: NgxCoolDialogsService,
    private loadingBar: LoadingBarService,
    private utilities: UtilitiesService,
    private broadcaster: BroadcasterService,
    private uploadService: UploadService,
    private _electronService: ElectronService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    for (let i = 0; i < 35; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.utilities.contextMenu = [];
  }

  ngOnInit() {
    this.pageImage = 1;
    this.imagesArr = [];
    this.showContentImage = false;
    this.showContentImageErr = false;
    this.showLoading = true;
    this.showUpload = false;
    this.showDragDropUpload = false;

    this.action = this.route.snapshot.paramMap.get("action");
    this.config.dictDefaultMessage = "Click Here or Drag & Drop files to instantly upload them.";
    this.config.parallelUploads = 1;

    this.loadImage(this.pageImage, this.perPage);
    this.generateImagePolicy();

    this.broadcaster.on<any>("getInfo").subscribe(() => {
      this.castContextMenuEvent("getInfo");
    });

    this.broadcaster.on<any>("delete").subscribe(() => {
      this.castContextMenuEvent("delete");
    });

    this.broadcaster.on<any>("download").subscribe(() => {
      this.castContextMenuEvent("download");
    });
  }

  loadImage(page, total) {
    console.log("loadImage");
    this.images
      .image_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] != false) {
            this.pageImage = parseInt(data.data.currentPage, 0);
            this.totalPageImage = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.images); i++) {
              data.data.images[i].selected = false;

              if (_.isEmpty(data.data.images[i].caption)) {
                data.data.images[i].caption = "No Caption";
              }
              if (_.isEmpty(data.data.images[i].location)) {
                data.data.images[i].location = "No Location";
              }

              this.imagesArr.push(data.data.images[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentImageErr = true;
            this.imageContentErr = {
              message: "No results found"
            };
          }
          this.showContentImage = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentImage = true;
          this.showContentImageErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.imageContentErr = error;
          }
        }
      );
  }

  formatBytes = (bytes, decimals) => {
    if (bytes == 0) return "0 Bytes";
    let k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  generateImagePolicy() {
    this.uploadService
      .upload_image_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_image_policy: ", data.data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }

          const vm = this;
          this.config.url = this.action_uri;
          (this.config.acceptedFiles = this.contentType + "*"),
            (this.config.paramName = this.file_name);
          this.config.autoProcessQueue = true;

          this.config.init = function() {
            const dz = this;
            dz.on("sending", function(file, xhr, formData) {
              formData.append("acl", vm.acl);
              formData.append("Policy", vm.policy);
              formData.append("X-Amz-Signature", vm.signature);
              formData.append("AWSAccessKeyId", vm.accessKey);
              formData.append("Signature", vm.signature);
              formData.append("success_action_status", vm.successActionStatus);
              formData.append("Content-Type", file.type);
              formData.append(
                "key",
                vm.file_key.split("/")[0] + "/" + file.name
              );
            });

            dz.on("addedfile", function(file) {
              if (file.size < environment.imageFileSize) {
                console.log("WARNING");
                vm.zone.run(() => {
                  vm.toastr.warning(
                    "Your proposed upload file is smaller than the minimum allowed size of " + vm.formatBytes(environment.imageFileSize, 0),
                    "WARNING"
                  );
                  setTimeout(() => {
                    vm.componentRef.dropzone().removeFile(file);
                  }, 600);
                });
                return;
              }
              vm.fileAdded += 1;
              console.log("addedfile: ", vm.fileAdded);
              vm.changeDetection.detectChanges();

              /*  if (vm._electronService.isElectronApp) {
                vm._electronService.ipcRenderer.send("show::progressBar");
              } */
            });

            dz.on("success", function(file) {
              setTimeout(() => {
                vm.fileAdded -= 1;
                console.log("success: ", vm.fileAdded);
                vm.componentRef.dropzone().removeFile(file);

                if (vm.fileAdded === 0) {
                  setTimeout(() => {
                    vm.zone.run(() => {
                      vm.showDragDropUpload = false;
                      vm.refresh();
                    });
                  }, 600);
                }
              }, 1000);
            });

            dz.on("uploadprogress", function(file, progress, bytesSent) {
              // Display the progress
              console.log("progress: ", progress);
              console.log("vm.fileAdded: ", vm.fileAdded);
              /* if (vm._electronService.isElectronApp) {
                vm._electronService.ipcRenderer.send("file::upload", {
                  progress: progress,
                  fileAdded: vm.fileAdded,
                  filename: file.name
                });
              } */
            });
          };
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  refresh() {
    this.pageImage = 1;
    this.imagesArr = [];
    this.showContentImage = false;
    this.showContentImageErr = false;

    this.loadImage(this.pageImage, this.perPage);
    this.generateImagePolicy();
  }

  viewMore() {
    if (this.action === "connect") {
    } else {
      this.pageImage = this.pageImage + 1;
      if (this.pageImage <= this.totalPageImage) {
        this.showLoading = true;
        this.loadImage(this.pageImage, this.perPage);
      }
    }
  }

  viewDetail(item: any) {
    const params: any = {
      queryParams: { imageId: item.id },
      queryParamsHandling: "merge"
    };
    this.router.navigate(["/image-details"], params);
  }

  selectMedia(item: any, event) {
    if (event.ctrlKey || event.metaKey) {
      if (_.find(this.selectedMedia, { id: item.id })) {
        item.selected = false;
        _.remove(this.selectedMedia, (row: any) => {
          return row.id === item.id;
        });
        this.utilities.selectedData = this.selectedMedia;
      } else {
        item.selected = true;
        this.selectedMedia.push(item);
        this.utilities.selectedData = this.selectedMedia;
      }
    } else {
      item.selected = !item.selected;
      _.each(this.imagesArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        }
      });

      if (item.selected) {
        this.selectedItem = item;
        this.utilities.selectedItem = item;
      } else {
        this.selectedItem = {};
        this.utilities.selectedItem = {};
      }
    }

    this.utilities.contextMenu = [];
    if (item.selected) {
      this.utilities.contextMenu = [
        {
          text: "View",
          key: "getInfo"
        },
        {
          text: "Delete",
          key: "delete"
        },
        {
          text: "Download",
          key: "download"
        }
      ];
    }
    this.contextMenu = this.utilities.contextMenu;
  }

  cancelUploadMedia() {
    this.showDragDropUpload = false;
    this.componentRef.dropzone().removeAllFiles(true);

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  uploadMedia() {
    if (this.showDragDropUpload) {
      this.toastr.warning(
        "Unable to proceed, file upload is on-going!",
        "WARNING"
      );
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "media",
      title: "Upload Image"
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "custom-modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  allowDrop(ev) {
    ev.preventDefault();
    if (!this.showContentImage) {
      return;
    }

    this.showDragDropUpload = true;
  }

  dropFile(event) {
    console.log("dndEnd dropFile ");
    event.preventDefault();
  }

  /*dragStart(event) {
    console.log("Element was dragged", event);
    event.dataTransfer.effectAllowed = "copy";
    if (this.selectedItem.selected) {
      if (this._electronService.isElectronApp) {
        this.downloadImage();
      }
    }
  }*/

  onUploadError(args: any): void {
    console.log("onUploadError:", args);
    if (args && _.isArray(args) && args[0].status === "error") {
      this.parseXML(args[1]).then((data: any) => {
        console.log("parseXML: ", data);
        if (data && data.Error) {
          this.toastr.error(data.Error.Message[0], "ERROR");
        } else {
          this.toastr.error(args[1], "ERROR");
        }
      });
      this.componentRef.dropzone().removeFile(args[0]);
      return;
    } else if (args && _.isArray(args) && args[0].status === "canceled") {
      this.toastr.warning(args[1], "WARNING");
      return;
    }
  }

  onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
    if (args && _.isArray(args) && args[0].status === "success") {
      this.toastr.success(args[1] || "File upload successfully!", "SUCCESS");
    }
  }

  onContextMenu($event: MouseEvent, item: any): void {
    if (item.selected) {
      this.contextMenuService.show.next({
        contextMenu: this.basicMenu,
        event: $event,
        item: item
      });
      this.utilities.selectedItem = item;
    }
    $event.preventDefault();
    $event.stopPropagation();
  }

  castContextMenuEvent(key: string) {
    if (key === "getInfo") {
      this.viewDetail(this.utilities.selectedItem);
    } else if (key === "delete") {
      if (_.size(this.utilities.selectedData) >= 2) {
        const selected = _.map(this.utilities.selectedData, (row: any) => {
          return row.id;
        });
        this.deleteImage(selected);
      } else {
        this.deleteImage([this.utilities.selectedItem.id]);
      }
    } else if (key === "download") {
      this.downloadImage();
    }
  }

  private deleteImage(item: any) {
    console.log("deleteImage: ", item);
    this.coolDialogs
      .confirm("Are you sure to delete this image?")
      .subscribe(res => {
        if (res) {
          /*this.loadingBar.progress$.subscribe(ev => {
            console.log("image_delete PROGRESS: ", ev);
          });*/
          this.toastr.info("Deleting...", "INFO");
          this.images.image_delete(this.token, item).then(
            (data: any) => {
              if (data && data.success) {
                console.log("image_delete: ", data);
                this.toastr.success("Image successfully deleted!", "SUCCESS");
                // this.loadingBar.progress$.unsubscribe();
                setTimeout(() => {
                  this.refresh();
                }, 600);
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  private downloadImage() {
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.toastr.info("Downloading ...");
    this.images.image_download(this.token, this.selectedItem.id).then(
      (data: any) => {
        if (data && data.success) {
          const filename =
            getFilename(data.data).filename + "." + getFilename(data.data).ext;
          if (this._electronService.isElectronApp) {
            const downloadFile: any = {
              file: data.data,
              filename: filename
            };
            console.log("downloadFile: ", downloadFile);
            this._electronService.ipcRenderer.sendSync(
              "file:download",
              downloadFile
            );
          } else {
            // download(data.data, filename);
            const downloadFile: any = {
              file: data.data,
              filename: filename
            };
            console.log("downloadFile: ", downloadFile);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  private parseXML(data) {
    return new Promise(resolve => {
      const parser = new xml2js.Parser({
        trim: true
      });

      parser.parseString(data, function(err, result) {
        console.log("result: ", result);
        resolve(result);
      });
    });
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
