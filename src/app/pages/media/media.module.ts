import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  TabsModule,
  BsDropdownModule,
  ButtonsModule,
  CollapseModule,
  ModalModule,
  CarouselModule
} from "ngx-bootstrap";
import { DropzoneModule } from "ngx-dropzone-wrapper";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ClipboardModule } from "ngx-clipboard";
import { DndModule } from "ngx-drag-drop";
import { ContextMenuModule } from "ngx-contextmenu";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { ImagesComponent } from "./images/images.component";
import { LogoComponent } from "./logo/logo.component";
import { PrintComponent } from "./print/print.component";
import { VideoComponent } from "./video/video.component";
import { MediaComponent } from "./media.component";
import { ImageDetailComponent } from "./images/image-detail/image-detail.component";
import { LogoDetailComponent } from "./logo/logo-detail/logo-detail.component";
import { PrintDetailComponent } from "./print/print-detail/print-detail.component";
import { VideoDetailComponent } from "./video/video-detail/video-detail.component";
import { ImageShareComponent } from "./sharer/image-share.component";
import { ImageEntryComponent } from "./images/image-entry/image-entry.component";

@NgModule({
  declarations: [
    ImagesComponent,
    LogoComponent,
    PrintComponent,
    VideoComponent,
    MediaComponent,
    ImageDetailComponent,
    LogoDetailComponent,
    PrintDetailComponent,
    VideoDetailComponent,
    ImageShareComponent,
    ImageEntryComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule,
    TabsModule,
    BsDropdownModule,
    ButtonsModule,
    CollapseModule,
    ModalModule,
    CarouselModule,
    DropzoneModule,
    FormsModule,
    RouterModule,
    ClipboardModule,
    DndModule,
    ContextMenuModule
  ],
  entryComponents: [
    ImageDetailComponent,
    VideoDetailComponent,
    PrintDetailComponent,
    LogoDetailComponent,
    ImageShareComponent
  ]
})
export class MediaModule {}
