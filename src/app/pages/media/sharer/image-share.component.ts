import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  NgZone,
  ViewChild
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { ImagesService } from "../../../services/images.service";
import { LogosService } from "../../../services/logos.service";
import { PrintService } from "../../../services/print.service";
import { VideosService } from "../../../services/videos.service";

@Component({
  selector: "app-image-share",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./image-share.component.html",
  styleUrls: ["./image-share.component.css"]
})
export class ImageShareComponent implements OnInit {
  token: string;
  imageId: string;
  logoId: string;
  printId: string;
  videoId: string;

  business: any = {};
  item: any = {};
  action: string;

  isLoading: boolean;

  constructor(
    private zone: NgZone,
    public toastr: ToastrService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private images: ImagesService,
    private logos: LogosService,
    private prints: PrintService,
    private videos: VideosService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.isLoading = true;
    if (this.action === "image") {
      this.generateShareImage();
    } else if (this.action === "logo") {
      this.generateShareLogo();
    } else if (this.action === "print") {
      this.generateSharePrint();
    } else if (this.action === "video") {
      this.generateShareVideo();
    }
  }

  private generateShareImage() {
    this.images.share_url_image(this.token, this.imageId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.item.link = data.data;
        }
        this.isLoading = false;
      },
      error => {
        console.log("error: ", error);
        this.isLoading = false;
      }
    );
  }

  private generateShareLogo() {
    this.logos.share_url_logo(this.token, this.logoId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.item.link = data.data;
        }
        this.isLoading = false;
      },
      error => {
        console.log("error: ", error);
        this.isLoading = false;
      }
    );
  }

  private generateSharePrint() {
    this.prints.share_url_print(this.token, this.printId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.item.link = data.data;
        }
        this.isLoading = false;
      },
      error => {
        console.log("error: ", error);
        this.isLoading = false;
      }
    );
  }

  private generateShareVideo() {
    this.videos.share_url_video(this.token, this.printId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          this.item.link = data.data;
        }
        this.isLoading = false;
      },
      error => {
        console.log("error: ", error);
        this.isLoading = false;
      }
    );
  }

  copied(event) {
    if (event.isSuccess) {
      this.toastr.info("Copied!");
    }
  }
}
