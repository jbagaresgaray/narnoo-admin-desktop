import {
  Component,
  OnInit,
  NgZone,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import * as $ from "jquery";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ElectronService } from "ngx-electron";
import { Location } from "@angular/common";

import { VideosService } from "../../../../services/videos.service";

import { ImageShareComponent } from "../../sharer/image-share.component";
import { UploadComponent } from "../../../upload/upload.component";

@Component({
  selector: "app-video-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./video-detail.component.html",
  styleUrls: ["./video-detail.component.css"]
})
export class VideoDetailComponent implements OnInit {
  token: string;
  action: string;
  videoId: string;

  item: any = {};
  params: any = {};
  business: any = {};
  connect_params: any = {};
  notif_business: any = {};

  isGeneral: boolean;
  isEmbededInfo: boolean;

  showError: boolean = false;
  showLoading: boolean = true;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    private videos: VideosService,
    private _electronService: ElectronService,
    public location: Location
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.isGeneral = true;
    this.route.queryParams.subscribe((params: any) => {
      console.log("params: ", params); // {order: "popular"}

      if (params.videoId) {
        this.showLoading = true;
        this.videoId = params.videoId;
        this.initData();
      }
    });
  }

  private initData(ev?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.item = data.data;

        if (_.isEmpty(this.item.caption)) {
          this.item.caption = "No caption available";
        }
        console.log("video: ", this.item);
      }
      this.showLoading = false;
      this.showError = false;
      if (ev) {
        ev.complete();
      }
    };

    const errorResponse = error => {
      this.showLoading = false;
      this.showError = true;
      if (ev) {
        ev.complete();
      }
    };

    this.videos
      .video_detail(this.token, this.videoId)
      .then(successResponse, errorResponse);
  }

  back() {
    this.location.back();
  }

  refresh() {
    this.showLoading = true;
    this.initData();
  }

  showTab(action) {
    if (this.showLoading) {
      return;
    }

    if (action === "general") {
      this.zone.run(() => {
        this.isGeneral = true;
        this.isEmbededInfo = false;
      });
    } else if (action === "embed") {
      this.zone.run(() => {
        this.isGeneral = false;
        this.isEmbededInfo = true;
      });
    }

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 300);
  }

  copied(event) {
    if (event.isSuccess) {
      this.toastr.info("Copied!");
    }
  }

  downloadVideo(file) {
    this.toastr.info("Downloading ...");
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const downloadFile = (data: any) => {
      const blob = new Blob([data], { type: "video/mp4" });
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.videos.video_download(this.token, this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data[file]: ", data.data[file]);
          const filename = getFilename(data.data[file]).filename;
          const fileType = getFilename(data.data[file]).ext;

          const _file = filename + "." + fileType;
          if (this._electronService.isElectronApp) {
            // DOWNLOAD NATIVE VIA ELECTRON
            download(data.data[file], _file);
            this.toastr.success("Video successfully downloaded", "SUCCESS");
          } else {
            download(data.data[file], _file);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  shareVideo() {
    const initialState: any = {
      videoId: this.item.id,
      action: "video"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(ImageShareComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteVideo() {
    this.coolDialogs
      .confirm("Are you sure to delete this video?")
      .subscribe(res => {
        if (res) {
          /* this.loadingBar.progress$.subscribe(ev => {
            console.log("image_delete PROGRESS: ", ev);
          }); */
          this.toastr.info("Deleting...", "INFO");
          this.videos.videos_delete(this.token, [this.item.id]).then(
            (data: any) => {
              if (data && data.success) {
                console.log("videos_delete: ", data);
                this.toastr.success("Video successfully deleted!", "SUCCESS");
                // this.loadingBar.progress$.unsubscribe();
                setTimeout(() => {
                  this.router.navigate(["/media"]);
                }, 600);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  uploadMedia(action) {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    let initialState: any = {};

    if (action === "thumb") {
      initialState = {
        action: "update_video_thumbnail_policy",
        title: "Upload Video Thumbnail",
        videoId: this.item.id
      };
    } else if (action === "file") {
      initialState = {
        action: "update_video_policy",
        title: "Upload Video"
      };
    }

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
