import { Component, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-media",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./media.component.html",
  styleUrls: ["./media.component.css"]
})
export class MediaComponent implements OnInit {
  isImage = true;
  isLogo = false;
  isPrint = false;
  isVideo = false;
  isAlbum = false;

  constructor() {}

  ngOnInit() {}

  showTab(action) {
    if (action === "image") {
      this.isImage = true;
      this.isLogo = false;
      this.isPrint = false;
      this.isVideo = false;
      this.isAlbum = false;
    } else if (action === "logo") {
      this.isImage = false;
      this.isLogo = true;
      this.isPrint = false;
      this.isVideo = false;
      this.isAlbum = false;
    } else if (action === "print") {
      this.isImage = false;
      this.isLogo = false;
      this.isPrint = true;
      this.isVideo = false;
      this.isAlbum = false;
    } else if (action === "video") {
      this.isImage = false;
      this.isLogo = false;
      this.isPrint = false;
      this.isVideo = true;
      this.isAlbum = false;
    } else if (action === "album") {
      this.isImage = false;
      this.isLogo = false;
      this.isPrint = false;
      this.isVideo = false;
      this.isAlbum = true;
    }
  }
}
