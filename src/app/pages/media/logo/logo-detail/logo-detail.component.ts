import { Component, OnInit, NgZone, ViewEncapsulation } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import * as $ from "jquery";
import * as _ from "lodash";
import { Location } from "@angular/common";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { ElectronService } from "ngx-electron";

import { LogosService } from "../../../../services/logos.service";
import { AlbumsService } from "../../../../services/albums.service";
import { ProductsService } from "../../../../services/products.service";

import { ImageShareComponent } from "../../sharer/image-share.component";

@Component({
  selector: "app-logo-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./logo-detail.component.html",
  styleUrls: ["./logo-detail.component.css"]
})
export class LogoDetailComponent implements OnInit {
  token: string;
  action: string;
  logoId: string;

  item: any = {};
  params: any = {};
  business: any = {};

  showDownload: boolean;
  showError: boolean = false;
  showLoading: boolean = true;

  isGeneral: boolean = false;
  isEmbededInfo: boolean = false;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public zone: NgZone,
    public toastr: ToastrService,
    private modalService: BsModalService,
    public coolDialogs: NgxCoolDialogsService,
    public logos: LogosService,
    private _electronService: ElectronService,
    public location: Location
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.isGeneral = true;

    this.route.queryParams.subscribe((params: any) => {
      console.log("params: ", params); // {order: "popular"}

      if (params.logoId) {
        this.showLoading = true;
        this.logoId = params.logoId;
        this.initData();
      }
    });
  }

  private initData(event?: any) {
    const successResponse = (data: any) => {
      if (data && data.success) {
        this.item = data.data;
        console.log("logo: ", this.item);
      }

      this.showLoading = false;
      this.showError = false;
      setTimeout(() => {
        $(".progressive-image").each(function() {
          const image = new Image();
          const previewImage = $(this).find(".loadingImage");
          const newImage = $(this).find(".overlay");

          image.onload = function() {
            newImage.css("background-image", "url(" + image.src + ")");
            newImage.css("opacity", "1");
          };
          image.onerror = function() {
            newImage.css("background-image", "url(./assets/img/thumb.jpg)");
            newImage.css("opacity", "1");
          };
          image.src = previewImage.data("image");
        });
      }, 1000);
      if (event) {
        event.complete();
      }
    };

    const errorResponse = error => {
      this.showLoading = false;
      this.showError = true;
      if (event) {
        event.complete();
      }
    };

    this.logos
      .logos_detail(this.token, this.logoId)
      .then(successResponse, errorResponse);
  }

  back() {
    this.location.back();
  }

  copied(event) {
    if (event.isSuccess) {
      this.toastr.info("Copied!");
    }
  }

  showTab(action) {
    if (this.showLoading) {
      return;
    }

    if (action === "general") {
      this.isGeneral = true;
      this.isEmbededInfo = false;
    } else if (action === "embed") {
      this.isGeneral = false;
      this.isEmbededInfo = true;
    }

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 300);
  }

  downloadImage() {
    this.toastr.info("Downloading ...");
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.logos.logos_download(this.token, this.item.id).then(
      (data: any) => {
        if (data && data.success) {
          let file: any = "";
          if (data.data && data.data.length > 0) {
            file = data.data[0];
          } else {
            file = data.data;
          }
          console.log("file: ", file);
          const filename =
            getFilename(file).filename + "." + getFilename(file).ext;

          if (this._electronService.isElectronApp) {
            download(file, filename);
            this.toastr.success("Logo successfully downloaded", "SUCCESS");
          } else {
            // DOWNLOAD NATIVE VIA ELECTRON
            download(file, filename);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  shareLogo() {
    const initialState: any = {
      logoId: this.item.id,
      action: "logo"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(ImageShareComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  deleteImage() {
    this.coolDialogs
      .confirm("Are you sure to delete this image?")
      .subscribe(res => {
        if (res) {
          /* this.loadingBar.progress$.subscribe(ev => {
            console.log("image_delete PROGRESS: ", ev);
          }); */
          this.toastr.info("Deleting...", "INFO");
          this.logos.logos_delete(this.token, [this.item.id]).then(
            (data: any) => {
              if (data && data.success) {
                console.log("logos_delete: ", data);
                this.toastr.success("Logo successfully deleted!", "SUCCESS");
                // this.loadingBar.progress$.unsubscribe();
                setTimeout(() => {
                  this.router.navigate(["/media"]);
                }, 600);
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  featureLogo() {
    this.coolDialogs
      .confirm("Are you sure to feature this logo?")
      .subscribe(res => {
        if (res) {
          this.toastr.info("Featuring...", "INFO");
          this.logos.logos_feature(this.token, this.item).then(
            (data: any) => {
              if (data && data.success) {
                console.log("logos_feature: ", data);
                this.toastr.success(data.data, "SUCCESS");
                setTimeout(() => {
                  this.initData();
                }, 600);
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }
}
