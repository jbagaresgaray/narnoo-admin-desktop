import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ChangeDetectorRef,
  NgZone
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import {
  DropzoneComponent,
  DropzoneDirective,
  DropzoneConfigInterface
} from "ngx-dropzone-wrapper";
import * as xml2js from "xml2js";
import * as _ from "lodash";
import * as $ from "jquery";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { LoadingBarService } from "@ngx-loading-bar/core";
import { combineLatest, Subscription } from "rxjs";
import { BroadcasterService } from "ng-broadcaster";
import { ContextMenuComponent, ContextMenuService } from "ngx-contextmenu";
import { ElectronService } from "ngx-electron";

import { LogosService } from "../../../services/logos.service";
import { UtilitiesService } from "../../../services/utilities.service";
import { UploadService } from "../../../services/upload.service";

import { UploadComponent } from "../../upload/upload.component";
import { LogoDetailComponent } from "./logo-detail/logo-detail.component";

import { environment } from "../../../../environments/environment";

@Component({
  selector: "app-media-logo",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./logo.component.html",
  styleUrls: ["./logo.component.css"]
})
export class LogoComponent implements OnInit {
  token: string;
  business: any = {};
  action: string;

  imagesLoadingArr: any[] = [];
  logosArr: any[] = [];
  selectedMedia: any[] = [];
  contextMenu: any[] = [];
  selectedItem: any = {};

  accessKey: string;
  action_uri: string;
  file_name: string;
  policy: string;
  signature: string;
  file_key: string;
  contentType: string;
  successActionStatus: string;
  acl: string;

  filters: any = {};
  showPreview: boolean = false;
  showUpload: boolean = false;
  showDragDropUpload = false;

  showContentLogo: boolean = false;
  showContentLogoErr: boolean = false;
  showLoading: boolean = false;

  logoContentErr: any = {};
  pageLogo = 0;
  totalPageLogo = 0;
  fileAdded = 0;

  perPage = 21;

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  @ViewChild(DropzoneDirective)
  componentRef?: DropzoneDirective;
  public config: DropzoneConfigInterface = {};

  @ViewChild(ContextMenuComponent)
  public basicMenu: ContextMenuComponent;

  constructor(
    private zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    public logos: LogosService,
    private modalService: BsModalService,
    private coolDialogs: NgxCoolDialogsService,
    private loadingBar: LoadingBarService,
    private contextMenuService: ContextMenuService,
    private broadcaster: BroadcasterService,
    public toastr: ToastrService,
    private utilities: UtilitiesService,
    private uploadService: UploadService,
    private _electronService: ElectronService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    for (let i = 0; i < 35; ++i) {
      this.imagesLoadingArr.push(i);
    }

    this.utilities.contextMenu = [];
  }

  ngOnInit() {
    this.pageLogo = 1;
    this.showContentLogo = false;
    this.showContentLogoErr = false;
    this.showLoading = true;
    this.showUpload = false;
    this.showDragDropUpload = false;

    this.action = this.route.snapshot.paramMap.get("action");
    this.config.dictDefaultMessage =
      "Click Here or Drag & Drop files to instantly upload them.";

    this.loadLogos(this.pageLogo, this.perPage);
    this.generateLogoPolicy();

    this.broadcaster.on<any>("getInfo").subscribe(() => {
      this.castContextMenuEvent("getInfo");
    });

    this.broadcaster.on<any>("delete").subscribe(() => {
      this.castContextMenuEvent("delete");
    });

    this.broadcaster.on<any>("download").subscribe(() => {
      this.castContextMenuEvent("download");
    });
  }

  loadLogos(page, total) {
    this.logos
      .logos_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.pageLogo = parseInt(data.data.currentPage, 0);
            this.totalPageLogo = parseInt(data.data.totalPages, 0);
            for (let i = 0; i < _.size(data.data.logos); i++) {
              data.data.logos[i].selected = false;
              this.logosArr.push(data.data.logos[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentLogoErr = true;
            this.logoContentErr = {
              message: "No results found"
            };
          }
          this.showContentLogo = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        error => {
          this.showContentLogo = true;
          this.showContentLogoErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.logoContentErr = error;
          }
        }
      );
  }

  formatBytes = (bytes, decimals) => {
    if (bytes == 0) return "0 Bytes";
    let k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  private generateLogoPolicy() {
    this.showLoading = true;
    this.uploadService
      .upload_logo_policy(this.token)
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("upload_logo_policy: ", data.data);
            this.accessKey = data.data.accessKey;
            this.action_uri = data.data.action_uri;
            this.file_name = data.data.name;
            this.file_key = data.data.key;
            this.policy = data.data.policy;
            this.signature = data.data.signature;
            this.contentType = data.data.contentType;
            this.successActionStatus = data.data.successActionStatus;
            this.acl = data.data.acl;
          }

          const vm = this;
          this.config.url = this.action_uri;
          (this.config.acceptedFiles = this.contentType + "*"),
            (this.config.paramName = this.file_name);
          this.config.autoProcessQueue = true;

          this.config.init = function() {
            const dz = this;
            dz.on("sending", function(file, xhr, formData) {
              formData.append("acl", vm.acl);
              formData.append("Policy", vm.policy);
              formData.append("X-Amz-Signature", vm.signature);
              formData.append("AWSAccessKeyId", vm.accessKey);
              formData.append("Signature", vm.signature);
              formData.append("success_action_status", vm.successActionStatus);
              formData.append("Content-Type", file.type);
              formData.append(
                "key",
                vm.file_key.split("/")[0] + "/" + file.name
              );
            });

            dz.on("addedfile", function(file) {
              if (file.size < environment.logoFileSize) {
                console.log("WARNING");
                vm.zone.run(() => {
                  vm.toastr.warning(
                    "Your proposed upload file is smaller than the minimum allowed size of " + vm.formatBytes(environment.imageFileSize, 0),
                    "WARNING"
                  );
                  setTimeout(() => {
                    vm.componentRef.dropzone().removeFile(file);
                  }, 600);
                });
                return;
              }
              vm.fileAdded += 1;
              console.log("addedfile: ", vm.fileAdded);
              vm.changeDetection.detectChanges();
            });

            dz.on("success", function(file) {
              setTimeout(() => {
                vm.fileAdded -= 1;
                console.log("success: ", vm.fileAdded);
                vm.componentRef.dropzone().removeFile(file);

                if (vm.fileAdded === 0) {
                  setTimeout(() => {
                    vm.zone.run(() => {
                      vm.showDragDropUpload = false;
                      vm.refresh();
                    });
                  }, 600);
                }
              }, 1000);
            });

            dz.on("uploadprogress", function(file, progress, bytesSent) {
              // Display the progress
              console.log("progress: ", progress);
            });
          };
        },
        error => {
          console.error("error: ", error);
          this.showLoading = false;
        }
      )
      .catch(error => {
        console.error("error: ", error);
        this.showLoading = false;
      });
  }

  refresh() {
    this.pageLogo = 1;
    this.logosArr = [];
    this.showContentLogo = false;
    this.showContentLogoErr = false;

    this.loadLogos(this.pageLogo, this.perPage);
    this.generateLogoPolicy();
  }

  viewDetail(item: any) {
    const params: any = {
      queryParams: { logoId: item.id },
      queryParamsHandling: "merge"
    };
    this.router.navigate(["/logo-details"], params);
  }

  viewMore() {
    if (this.action === "connect") {
    } else {
      this.pageLogo = this.pageLogo + 1;
      if (this.pageLogo <= this.totalPageLogo) {
        this.showLoading = true;
        this.loadLogos(this.pageLogo, this.perPage);
      }
    }
  }

  selectMedia(item: any, event) {
    if (event.ctrlKey || event.metaKey) {
      if (_.find(this.selectedMedia, { id: item.id })) {
        item.selected = false;
        _.remove(this.selectedMedia, (row: any) => {
          return row.id === item.id;
        });
        this.utilities.selectedData = this.selectedMedia;
      } else {
        item.selected = true;
        this.selectedMedia.push(item);
        this.utilities.selectedData = this.selectedMedia;
      }
    } else {
      item.selected = !item.selected;
      _.each(this.logosArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        }
      });
      if (item.selected) {
        this.selectedItem = item;
        this.utilities.selectedItem = item;
      } else {
        this.selectedItem = {};
        this.utilities.selectedItem = {};
      }
    }

    this.utilities.contextMenu = [];
    if (item.selected) {
      this.utilities.contextMenu = [
        {
          text: "View",
          key: "getInfo"
        },
        {
          text: "Delete",
          key: "delete"
        },
        {
          text: "Download",
          key: "download"
        }
      ];
    }
    this.contextMenu = this.utilities.contextMenu;
  }

  cancelUploadMedia() {
    this.showDragDropUpload = false;
    this.componentRef.dropzone().removeAllFiles(true);

    setTimeout(() => {
      $(".progressive-image").each(function() {
        const image = new Image();
        const previewImage = $(this).find(".loadingImage");
        const newImage = $(this).find(".overlay");
        image.src = previewImage.data("image");
        image.onload = function() {
          newImage.css("background-image", "url(" + image.src + ")");
          newImage.css("opacity", "1");
        };
        image.onerror = function() {
          newImage.css("background-image", "url(../assets/img/thumb.jpg)");
          newImage.css("opacity", "1");
        };
      });
    }, 1000);
  }

  uploadMedia() {
    if (this.showDragDropUpload) {
      this.toastr.warning(
        "Unable to proceed, file upload is on-going!",
        "WARNING"
      );
      return;
    }

    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.refresh();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "logos",
      title: "Upload Logos"
    };

    const modalConfig: any = {
      animated: true,
      keyboard: false,
      backdrop: "static",
      class: "custom-modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(UploadComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  allowDrop(ev) {
    ev.preventDefault();
    if (!this.showContentLogo) {
      return;
    }

    this.showDragDropUpload = true;
  }

  drop(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData("text");
    console.log("drop data: ", data);
  }

  onUploadError(args: any): void {
    console.log("onUploadError:", args);
    if (args && _.isArray(args) && args[0].status === "error") {
      this.parseXML(args[1]).then((data: any) => {
        console.log("parseXML: ", data);
        if (data && data.Error) {
          this.toastr.error(data.Error.Message[0], "ERROR");
        } else {
          this.toastr.error(args[1], "ERROR");
        }
      });
      this.componentRef.dropzone().removeFile(args[0]);
      return;
    } else if (args && _.isArray(args) && args[0].status === "canceled") {
      this.toastr.warning(args[1], "WARNING");
      return;
    }
  }

  onUploadSuccess(args: any): void {
    console.log("onUploadSuccess:", args);
    if (args && _.isArray(args) && args[0].status === "success") {
      this.toastr.success(args[1] || "File upload successfully!", "SUCCESS");
    }
  }

  onContextMenu($event: MouseEvent, item: any): void {
    if (item.selected) {
      this.contextMenuService.show.next({
        contextMenu: this.basicMenu,
        event: $event,
        item: item
      });
      this.utilities.selectedItem = item;
    }
    $event.preventDefault();
    $event.stopPropagation();
  }

  castContextMenuEvent(key: string) {
    if (key === "getInfo") {
      this.viewDetail(this.utilities.selectedItem);
    } else if (key === "delete") {
      if (_.size(this.utilities.selectedData) >= 2) {
        const selected = _.map(this.utilities.selectedData, (row: any) => {
          return row.id;
        });
        this.deleteImage(selected);
      } else {
        this.deleteImage([this.utilities.selectedItem.id]);
      }
    } else if (key === "download") {
      this.downloadImage();
    }
  }

  private deleteImage(item: any) {
    console.log("deleteImage: ", item);
    this.coolDialogs
      .confirm("Are you sure to delete this logo?")
      .subscribe(res => {
        if (res) {
          this.loadingBar.progress$.subscribe(ev => {
            console.log("logos_delete PROGRESS: ", ev);
          });
          this.toastr.info("Deleting...", "INFO");
          this.logos.logos_delete(this.token, item).then(
            (data: any) => {
              if (data && data.success) {
                console.log("logos_delete: ", data);
                this.toastr.success("Logo successfully deleted!", "SUCCESS");

                this.loadingBar.progress$.unsubscribe();
                setTimeout(() => {
                  this.refresh();
                }, 600);
              }
            },
            (error: any) => {
              console.log("error: ", error);
            }
          );
        }
      });
  }

  private downloadImage() {
    const download = (dataurl, filename) => {
      const a = document.createElement("a");
      a.href = dataurl;
      a.setAttribute("download", filename);
      const b = document.createEvent("MouseEvents");
      b.initEvent("click", false, true);
      a.dispatchEvent(b);
      return false;
    };

    const getFilename = url => {
      url = url
        .split("/")
        .pop()
        .replace(/\#(.*?)$/, "")
        .replace(/\?(.*?)$/, "");
      url = url.split("."); // separates filename and extension
      return { filename: url[0] || "", ext: url[1] || "" };
    };

    this.toastr.info("Downloading ...");
    this.logos.logos_download(this.token, this.selectedItem.id).then(
      (data: any) => {
        if (data && data.success) {
          let file: any = "";
          if (data.data && data.data.length > 0) {
            file = data.data[0];
          } else {
            file = data.data;
          }
          console.log("file: ", file);
          const filename =
            getFilename(file).filename + "." + getFilename(file).ext;

          if (this._electronService.isElectronApp) {
            const downloadFile: any = {
              file: file,
              filename: filename
            };
            console.log("downloadFile: ", downloadFile);
            this._electronService.ipcRenderer.sendSync(
              "file:download",
              downloadFile
            );
          } else {
            // download(file, filename);
            const downloadFile: any = {
              file: file,
              filename: filename
            };
            console.log("downloadFile: ", downloadFile);
          }
        }
      },
      error => {
        if (error && !error.success) {
          this.toastr.error(
            "Error while downloading file. " + error.message,
            "ERROR"
          );
          return;
        }
      }
    );
  }

  private parseXML(data) {
    return new Promise(resolve => {
      const parser = new xml2js.Parser({
        trim: true
      });

      parser.parseString(data, function(err, result) {
        console.log("result: ", result);
        resolve(result);
      });
    });
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
