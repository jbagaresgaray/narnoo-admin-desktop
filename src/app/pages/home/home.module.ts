import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BsDropdownModule, PaginationModule } from "ngx-bootstrap";
import { FormsModule } from "@angular/forms";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";
import { NgSelectModule } from "@ng-select/ng-select";

import { HomeComponent } from "./home.component";
import { CreateOperatorComponent } from "./create-operator/create-operator.component";

@NgModule({
  declarations: [HomeComponent, CreateOperatorComponent],
  imports: [
    CommonModule,
    PipesModule,
    ComponentsModule,
    BsDropdownModule,
    PaginationModule,
    FormsModule,
    NgSelectModule
  ]
})
export class HomeModule {}
