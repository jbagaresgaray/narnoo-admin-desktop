import { Component, OnInit, NgZone, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import * as _ from "lodash";
import * as CryptoJS from "crypto-js";

import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { UsersService } from "../../services/users.service";
import { ConnectService } from "../../services/connect.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "app-home",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  businessArr: any[] = [];
  searchArr: any[] = [];
  fakeArr: any[] = [];

  contextMenu: any[] = [];
  currentUser: any = {};

  showContent = false;
  showSearch = false;
  isGrid = false;

  profilePicture: string;

  selectedItem: any = {};
  users: any = {};
  _appVersion: any = {};

  currentPage: number;
  totalCount: number = 0;
  totalPage: number = 0;
  txtSearch: string;

  constructor(
    private zone: NgZone,
    private router: Router,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private services: UsersService,
    private utilities: UtilitiesService,
    private connect: ConnectService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("app.adminData"));
    for (let i = 0; i < 20; i++) {
      this.fakeArr.push(i);
    }

    this.currentPage = 1;

    this.initData();
    this.getUser();
  }

  private getUser() {
    this.users = JSON.parse(localStorage.getItem("app.adminData")) || {};
    console.log("this.users: ", this.users);

    if (this.users.email) {
      this.profilePicture =
        "https://www.gravatar.com/avatar/" +
        CryptoJS.MD5(this.users.email.toLowerCase());
    } else {
      this.profilePicture = this.utilities.radomizeAvatar();
    }
    console.log("this.profilePicture: ", this.profilePicture);
  }

  private initData() {
    this.showContent = false;
    this.showSearch = false;
    this.services
      .staffbusinesses({
        page: this.currentPage
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.businessArr = [];

            if (_.isArray(data.data)) {
              this.businessArr = data.data;
              this.totalCount = data.totalCount;
              this.totalPage = data.totalPage;

              _.each(this.businessArr, (row: any) => {
                if (row.type === 1) {
                  row.businessType = "Operator";
                } else if (row.type === 2) {
                  row.businessType = "Distributor";
                }

                if (row.role === 1) {
                  row.businessRole = "Administrator";
                } else if (row.role === 2) {
                  row.businessRole = "Staff";
                } else if (row.role === 3) {
                  row.businessRole = "Trade";
                } else if (row.role === 4) {
                  row.businessRole = "Media";
                }
                row.selected = false;
              });
            }
          }
          this.showContent = true;
          this.showSearch = false;
        },
        error => {
          console.log("error: ", error);
          this.showContent = true;
          this.showSearch = false;
          if (error && !error.success && error.error === "Token has expired") {
            this.router.navigate(["/login"]);
          }
        }
      );
  }

  private searchBusiness() {
    this.showContent = false;
    this.showSearch = false;
    this.services
      .search_staffbusinesses({
        name: this.txtSearch
      })
      .then(
        (data: any) => {
          console.log("staffbusinesses: ", data);
          this.searchArr = [];
          if (data && data.success) {
            if (_.isArray(data.data) && !_.isBoolean(data.data[0])) {
              this.currentPage = data.currentPage;
              this.searchArr = data.data;
              _.each(this.searchArr, (row: any) => {
                row.type = _.upperFirst(row.type);
                row.selected = false;
              });
            }
          }
          this.showContent = false;
          this.showSearch = true;
        },
        (error: any) => {
          console.log("error: ", error);
          this.showContent = false;
          this.showSearch = true;
        }
      );
  }

  refresh() {
    this.initData();
  }

  openProfile() { }

  openDashboard(item: any) {
    localStorage.setItem("admin.bus.token", item.token);
    localStorage.setItem("admin.business", JSON.stringify(item));
    this.router.navigate(["/user-profile"]);
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  logOutApp() {
    this.coolDialogs
      .confirm("Are you sure to logout application?")
      .subscribe(res => {
        if (res) {
          localStorage.removeItem("app.adminData");
          localStorage.removeItem("app.admintoken");
          localStorage.removeItem("admin.bus.token");
          localStorage.removeItem("admin.business");

          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 600);
        }
      });
  }

  createOperator() {
    this.router.navigate(["create"]);
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      console.log("searchBusiness");
      this.searchBusiness();
    }
  }

  pageChanged(event: any): void {
    console.log("Page changed to: " + event.page);
    console.log("Number items per page: " + event.itemsPerPage);
    this.currentPage = event.page;
    this.initData();
  }
}
