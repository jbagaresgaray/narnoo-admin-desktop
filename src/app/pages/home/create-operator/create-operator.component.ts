import { BusinessService } from "./../../../services/business.service";
import { Component, OnInit, ViewEncapsulation, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import * as _ from "lodash";
import { ElectronService } from "ngx-electron";

import { UtilitiesService } from "../../../services/utilities.service";
import { ConnectService } from "../../../services/connect.service";
import { UsersService } from "../../../services/users.service";

import { environment } from "../../../../environments/environment";

declare let $: any;

@Component({
  selector: "app-create-operator",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./create-operator.component.html",
  styleUrls: ["./create-operator.component.css"]
})
export class CreateOperatorComponent implements OnInit {
  isStepOne: boolean = true;
  isStepTwo: boolean = false;
  isStepThree: boolean = false;
  isStepFour: boolean = false;
  isStepFive: boolean = false;

  isAgree: boolean = false;
  isSearching: boolean = false;
  isRequesting: boolean = false;

  users: any = {};
  business: any = {};
  typeValue: any = {};

  stepCondition: boolean = false;
  isSameAddress: boolean = false;
  needAccess: boolean = false;
  showSubcats: boolean = false;
  showOperator: boolean = false;
  showDistributor: boolean = false;

  distributorCat: any[] = [];
  operatorCat: any[] = [];
  platformsArr: any[] = [];
  subCategoryArr: any[] = [];
  usersArr: any[] = [];
  countriesArr: any[] = [];

  constructor(
    public zone: NgZone,
    private router: Router,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private utilities: UtilitiesService,
    private businesses: BusinessService,
    private services: UsersService,
    private connect: ConnectService,
    private _electronService: ElectronService
  ) {}

  ngOnInit() {
    this.isStepOne = true;
    this.isStepTwo = false;
    this.isStepThree = false;
    this.isStepFour = false;
    this.isStepFive = false;

    this.utilities.getCountries().then(
      (data: any) => {
        if (data && data.success) {
          this.countriesArr = data.data;
        }
      },
      error => {
        console.log("error: ", error);
      }
    );

    this.utilities.category().then((data: any) => {
      if (data && data.success) {
        const operatorCat = [];
        _.each(data.data, (row: any) => {
          operatorCat.push({
            id: row.id,
            text: row.title
          });
        });
        this.operatorCat = operatorCat;
      }
      console.log("operatorCat: ", this.operatorCat);
    });

    this.utilities.distributor_category().then((data: any) => {
      if (data && data.success) {
        const distributorCat = [];
        for (let index = 0; index < data.data.length; index++) {
          const element = data.data[index];
          distributorCat.push({
            id: index + 1,
            text: element.name
          });
        }
        this.distributorCat = distributorCat;
      }
      console.log("distributorCat: ", this.distributorCat);
    });

    this.utilities.booking_platforms().then((data: any) => {
      if (data && data.success) {
        const platformsArr = [];
        for (let index = 0; index < data.data.length; index++) {
          const element = data.data[index];
          platformsArr.push({
            id: index + 1,
            text: element.name
          });
        }
        this.platformsArr = platformsArr;
      }
      console.log("platformsArr: ", this.platformsArr);
    });
  }

  private populateSubCategory(category) {
    this.utilities.subcategory(category).then(
      (data: any) => {
        if (data && data.success) {
          this.subCategoryArr = data.data;
          this.showSubcats = true;
        }
        this.stepCondition = true;
        console.log("this.subCategoryArr: ", this.subCategoryArr);
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  openHome() {
    this.router.navigate(["/home"]);
  }

  logOutApp() {
    this.coolDialogs
      .confirm("Are you sure to logout application?")
      .subscribe(res => {
        if (res) {
          localStorage.removeItem("app.adminData");
          localStorage.removeItem("app.admintoken");
          localStorage.removeItem("admin.bus.token");
          localStorage.removeItem("admin.business");

          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 600);
        }
      });
  }

  previous() {
    if (this.isStepOne) {
      this.isStepOne = true;
      this.isStepTwo = false;
      this.isStepThree = false;
      this.isStepFour = false;
      this.isStepFive = false;
      return;
    } else if (this.isStepTwo) {
      this.isStepOne = true;
      this.isStepTwo = false;
      this.isStepThree = false;
      this.isStepFour = false;
      this.isStepFive = false;
    } else if (this.isStepThree) {
      this.isStepOne = false;
      this.isStepTwo = true;
      this.isStepThree = false;
      this.isStepFour = false;
      this.isStepFive = false;
    } else if (this.isStepFour) {
      this.isStepOne = false;
      this.isStepTwo = false;
      this.isStepThree = true;
      this.isStepFour = false;
      this.isStepFive = false;
    } else if (this.isStepFive) {
      this.isStepOne = false;
      this.isStepTwo = false;
      this.isStepThree = false;
      this.isStepFour = true;
      this.isStepFive = false;
    }
  }

  next() {
    if (!this.stepCondition) {
      return;
    }

    if (this.isStepOne) {
      this.isStepOne = false;
      this.isStepTwo = true;
      this.isStepThree = false;
      this.isStepFour = false;
      this.isStepFive = false;

      this.stepCondition = false;
    } else if (this.isStepTwo) {
      this.isStepOne = false;
      this.isStepTwo = false;
      this.isStepThree = true;
      this.isStepFour = false;
      this.isStepFive = false;

      this.stepCondition = false;
    } else if (this.isStepThree) {
      this.isStepOne = false;
      this.isStepTwo = false;
      this.isStepThree = false;
      this.isStepFour = true;
      this.isStepFive = false;

      this.stepCondition = false;
    } else if (this.isStepFour) {
      this.isStepOne = false;
      this.isStepTwo = false;
      this.isStepThree = false;
      this.isStepFour = false;
      this.isStepFive = true;

      this.stepCondition = false;

      if (this.isSameAddress) {
        this.valildatePhyDetails(null);
      }
    }
  }

  showOperatorSelect() {
    this.showOperator = !this.showOperator;
    if (this.showOperator) {
      this.showDistributor = false;
      this.business.type = "Operator";
    }
  }

  showDistributorSelect() {
    this.showDistributor = !this.showDistributor;
    if (this.showDistributor) {
      this.showOperator = false;
      this.business.type = "Distributor";
    }
  }

  selectedValue(value: any): void {
    console.log("Selected value is: ", value);
    if (value) {
      // this.business.categoryObj = value;
      this.business.category = value.id;

      if (this.showOperator) {
        this.populateSubCategory(value.id);
      } else {
        this.stepCondition = true;
      }
      console.log("business: ", this.business);
    } else {
      this.stepCondition = false;
    }
  }

  viewTerms(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    if (this._electronService.isElectronApp) {
      this._electronService.ipcRenderer.sendSync(
        "open:link",
        environment.AppTerms
      );
    } else {
      window.open(environment.AppTerms, "_blank");
    }
  }

  toggleAddress(e) {
    console.log("isSameAddress: ", this.isSameAddress);
    if (this.isSameAddress) {
      this.business.physicalNumber = this.business.postalNumber;
      this.business.physicalStreet = this.business.postalStreet;
      this.business.physicalSuburb = this.business.postalSuburb;
      this.business.physicalState = this.business.postalState;
      this.business.physicalPostcode = this.business.postalPostcode;
      this.business.physicalCountry = this.business.postalCountry;

      this.valildateBusDetails(null);
    }
  }

  onKeydown(ev: any) {
    if (ev.key === "Enter") {
      this.validateBusinessName();
    }
  }

  validateBusinessName() {
    console.log("validateBusinessName:");

    const checkBusinessName = () => {
      this.isSearching = true;
      this.connect
        .search_businesses({
          name: this.business.name
        })
        .then(
          (data: any) => {
            this.usersArr = [];
            if (data && data.success) {
              if (
                _.isArray(data.data) &&
                _.isBoolean(data.data[0]) &&
                data.data[0] === false
              ) {
                this.stepCondition = true;
                this.needAccess = false;
                this.isSearching = false;
              } else if (
                _.isArray(data.data) &&
                _.isBoolean(data.data[0]) &&
                data.data[0] !== false
              ) {
                this.needAccess = false;
                this.stepCondition = false;
                this.isSearching = false;
              } else if (
                _.isArray(data.data) &&
                !_.isBoolean(data.data[0]) &&
                data.data[0] !== false
              ) {
                this.needAccess = true;
                this.stepCondition = false;
                this.isSearching = false;

                this.usersArr = data.data;
              } else {
                this.needAccess = true;
                this.stepCondition = false;
                this.isSearching = false;
              }
            } else {
              this.stepCondition = true;
              this.isSearching = false;
            }
          },
          error => {
            this.needAccess = true;
            this.stepCondition = false;
            this.isSearching = false;
          }
        );
    };

    /*if (!this.isAgree) {
      this.stepCondition = false;
    } else {
      if (this.business.name && this.business.name.trim() !== "") {
      setTimeout(() => {
      checkBusinessName();
      }, 1000);
      } else {
      this.stepCondition = false;
      }
    }*/
    if (this.business.name && this.business.name.trim() !== "") {
      setTimeout(() => {
        checkBusinessName();
      }, 1000);
    } else {
      this.stepCondition = false;
    }
  }

  valildateBusDetails(e) {
    console.log("valildateBusDetails");
    if (this.showSubcats) {
      if (
        this.business.subCategory &&
        this.business.bookingPlatform &&
        this.business.businessContact &&
        this.business.url &&
        this.business.email &&
        this.business.phone
      ) {
        this.stepCondition = true;
      } else {
        this.stepCondition = false;
      }
    } else {
      if (
        this.business.businessContact &&
        this.business.url &&
        this.business.email &&
        this.business.phone
      ) {
        this.stepCondition = true;
      } else {
        this.stepCondition = false;
      }
    }
  }

  valildatePostDetails(e) {
    if (
      this.business.postalNumber &&
      this.business.postalStreet &&
      this.business.postalSuburb &&
      this.business.postalState &&
      this.business.postalPostcode &&
      this.business.postalCountry
    ) {
      this.stepCondition = true;
    } else {
      this.stepCondition = false;
    }
  }

  valildatePhyDetails(e) {
    if (
      this.business.physicalNumber &&
      this.business.physicalStreet &&
      this.business.physicalSuburb &&
      this.business.physicalState &&
      this.business.physicalPostcode &&
      this.business.physicalCountry
    ) {
      this.stepCondition = true;
    } else {
      this.stepCondition = false;
    }
  }

  requestAccess(business) {
    console.log("business: ", business);
    const ctrl = this;

    const requestAccess = () => {
      business.isRequesting = true;
      ctrl.services
        .request_access({ accountId: business.id, type: business.type })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.toastr.success(data.message, "SUCCESS");
              this.isRequesting = false;
            } else {
              this.toastr.warning(data.message, "WARNING");
              this.isRequesting = false;
            }
            business.isRequesting = false;
          },
          error => {
            console.log("error: ", error);
            business.isRequesting = false;
          }
        );
    };

    this.coolDialogs
      .confirm("Do you want to request access to this business account?")
      .subscribe(res => {
        if (res) {
          requestAccess();
        }
      });
  }

  createBusiness() {
    console.log("this.business: ", this.business);

    const createBusiness = () => {
      this.toastr.info("Saving...", "INFO");
      this.businesses.create_business(this.business).then(
        (data: any) => {
          if (data && data.success) {
            this.toastr.success(data.data, "Record Saved!!");
            setTimeout(() => {
              this.router.navigate(["/home"]);
            }, 600);
          } else {
            this.toastr.warning(data.message, "ERROR!!");
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
    };

    this.coolDialogs
      .confirm("Do you want to proceed in creating business account?")
      .subscribe(res => {
        if (res) {
          createBusiness();
        }
      });
  }
}
