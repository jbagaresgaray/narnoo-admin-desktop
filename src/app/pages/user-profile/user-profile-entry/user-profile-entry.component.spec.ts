import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserProfileEntryComponent } from './user-profile-entry.component';

describe('UserProfileEntryComponent', () => {
  let component: UserProfileEntryComponent;
  let fixture: ComponentFixture<UserProfileEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
