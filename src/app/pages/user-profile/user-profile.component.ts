import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
  NgZone,
  ViewChild,
  ElementRef
} from "@angular/core";
import * as CryptoJS from "crypto-js";
import * as _ from "lodash";
import * as async from "async";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { UtilitiesService } from "../../services/utilities.service";
import { BusinessService } from "../../services/business.service";
import { ConnectService } from "../../services/connect.service";

import { UserProfileEntryComponent } from "./user-profile-entry/user-profile-entry.component";
import { BookingPlatformsComponent } from "./booking-platforms/booking-platforms.component";
import { OperatorPlatformsComponent } from "./operator-platforms/operator-platforms.component";

declare const google: any;

@Component({
  selector: "app-user-profile",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"]
})
export class UserProfileComponent implements OnInit {
  token: string;
  showContent: boolean;
  business: any = {};
  profile: any = {};

  followerCount: number;
  followingCount: number;
  followingArr: any[] = [];

  map: any;
  @ViewChild("map") mapElement: ElementRef;
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private zone: NgZone,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private businesses: BusinessService,
    private utilities: UtilitiesService,
    private connect: ConnectService
  ) {
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    this.token = localStorage.getItem("admin.bus.token");
  }

  ngOnInit() {
    this.followerCount = 0;
    this.followingCount = 0;

    this.initData();
  }

  private initData() {
    this.showContent = false;
    this.businesses.business_profile(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.profile = data.data;
          this.profile.gravatar =
            "https://www.gravatar.com/avatar/" +
            CryptoJS.MD5(this.profile.email.toLowerCase(), "hex");

          const keywords = data.data.keywords;
          if (keywords) {
            this.profile.tags = _.map(keywords.split(","), (row: any) => {
              return {
                display: row,
                value: row
              };
            });
          } else {
            this.profile.tags = null;
          }
          console.log("this.info : ", this.profile);

          async.parallel([
            (callback: any) => {
              this.connect.connect_followers(this.token).then(
                (data: any) => {
                  if (data && data.success) {
                    const result = data.data.data;
                    this.followerCount = _.size(result);
                    console.log("this.followerCount: ", this.followerCount);
                  }
                  callback();
                },
                error => {
                  console.log("error: ", error);
                  this.followerCount = 0;
                  callback();
                }
              );
            },
            (callback: any) => {
              this.connect.connect_following(this.token).then(
                (data: any) => {
                  if (data && data.success) {
                    const result = data.data.data;
                    this.followingCount = _.size(result);
                    this.followingArr = result;
                    console.log("this.followingCount: ", this.followingCount);
                  }
                  callback();
                },
                error => {
                  console.log("error: ", error);
                  this.followingCount = 0;
                  this.followingArr = [];
                  callback();
                }
              );
            }
          ]);

          // this.loadInteractiveMap(
          //   this.profile.latitude,
          //   this.profile.longitude
          // );
        }
        this.showContent = true;
      },
      (error: any) => {
        console.log("error: ", error);
        this.showContent = true;
      }
    );
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  

  updateProfile() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const modalConfig: any = {
      animated: true,
      class: "modal-lg"
    };

    this.bsModalRef = this.modalService.show(
      UserProfileEntryComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  editProfilePlatforms(action) {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: action
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(
      BookingPlatformsComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  viewOperatorPlatforms() {
    const modalConfig: any = {
      animated: true,
      class: "modal-lg"
    };

    this.bsModalRef = this.modalService.show(
      OperatorPlatformsComponent,
      modalConfig
    );
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
