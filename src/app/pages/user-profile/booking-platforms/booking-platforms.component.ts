import {
  Component,
  OnInit,
  ViewEncapsulation,
  NgZone,
  ViewChild,
  ElementRef
} from "@angular/core";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import * as async from "async";

import { BusinessService } from "../../../services/business.service";
import { ByobService } from "../../../services/byob.service";

@Component({
  selector: "app-booking-platforms",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./booking-platforms.component.html",
  styleUrls: ["./booking-platforms.component.css"]
})
export class BookingPlatformsComponent implements OnInit {
  token: string;
  action: string;

  profile: any = {};
  business: any = {};

  platformsArr: any[] = [];
  isLoading: boolean = true;

  isSaving: boolean = false;
  isReservation: boolean = false;
  isBooking: boolean = false;

  constructor(
    private zone: NgZone,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private businesses: BusinessService,
    public byob: ByobService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    console.log("action: ", this.action);
    this.isLoading = true;
    this.byob.admin_reservation_platforms(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.platformsArr = data.data.response;
          console.log("platformsArr: ", this.platformsArr);
        }
        this.isLoading = false;
      },
      error => {
        console.log("error: ", error);
        this.isLoading = false;
      }
    );
  }

  onBusinessType(ev) {}

  showTab(tab) {
    if (tab === "reservation") {
      this.isReservation = true;
      this.isBooking = false;
    } else if (tab === "booking") {
      this.isReservation = false;
      this.isBooking = true;
    }
  }

  saveChanges() {
    if (this.action === "reservation") {
      this.coolDialogs
        .confirm("Map operator platforms for reservation?")
        .subscribe(res => {
          if (res) {
            console.log("profile: ", this.profile);

            this.toastr.info("Mapping Platforms...", "INFO");
            this.isSaving = true;
            this.byob
              .admin_map_platforms(this.token, {
                operator: this.business.id,
                platform: this.profile.platform,
                platformId: this.profile.platformId
              })
              .then(
                (data: any) => {
                  if (data && data.success) {
                    console.log("success: ", data);
                    this.toastr.success("Reservation mapped successfully!", "SUCCESS");
                    this.modalService.setDismissReason("save");
                    setTimeout(() => {
                      this.bsModalRef.hide();
                    }, 600);
                  } else if (data && !data.success) {
                    this.toastr.warning(data.message, "WARNING");
                  }
                  this.isSaving = false;
                },
                error => {
                  this.isSaving = false;
                }
              );
          }
        });
    } else if (this.action === "booking") {
      this.coolDialogs
        .confirm("Set operator preferred booking system?")
        .subscribe(res => {
          if (res) {
            console.log("profile: ", this.profile);

            this.toastr.info("Setting Up Booking System...", "INFO");
            this.isSaving = true;
            this.byob
              .admin_map_operator_preference(this.token, {
                operator: this.business.id,
                platform: this.profile.platform
              })
              .then(
                (data: any) => {
                  if (data && data.success) {
                    console.log("success: ", data);
                    this.toastr.success("Booking system setup successfully!", "SUCCESS");
                    this.modalService.setDismissReason("save");
                    setTimeout(() => {
                      this.bsModalRef.hide();
                    }, 600);
                  } else if (data && !data.success) {
                    this.toastr.warning(data.message, "WARNING");
                  }
                  this.isSaving = false;
                },
                error => {
                  this.isSaving = false;
                }
              );
          }
        });
    }
  }
}
