import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingPlatformsComponent } from './booking-platforms.component';

describe('BookingPlatformsComponent', () => {
  let component: BookingPlatformsComponent;
  let fixture: ComponentFixture<BookingPlatformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingPlatformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingPlatformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
