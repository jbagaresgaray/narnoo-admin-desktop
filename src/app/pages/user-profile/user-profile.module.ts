import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ModalModule, BsDropdownModule } from "ngx-bootstrap";
import { TagInputModule } from "ngx-chips";
import { NgSelectModule } from "@ng-select/ng-select";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { UserProfileComponent } from "./user-profile.component";
import { UserProfileEntryComponent } from "./user-profile-entry/user-profile-entry.component";
import { BookingPlatformsComponent } from "./booking-platforms/booking-platforms.component";
import { OperatorPlatformsComponent } from "./operator-platforms/operator-platforms.component";

@NgModule({
  declarations: [
    UserProfileComponent,
    UserProfileEntryComponent,
    BookingPlatformsComponent,
    OperatorPlatformsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule,
    BsDropdownModule,
    ComponentsModule,
    PipesModule,
    TagInputModule,
    NgSelectModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    UserProfileEntryComponent,
    BookingPlatformsComponent,
    OperatorPlatformsComponent
  ]
})
export class UserProfileModule {}
