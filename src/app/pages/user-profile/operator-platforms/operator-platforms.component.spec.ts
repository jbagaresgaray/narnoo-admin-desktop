import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatorPlatformsComponent } from './operator-platforms.component';

describe('OperatorPlatformsComponent', () => {
  let component: OperatorPlatformsComponent;
  let fixture: ComponentFixture<OperatorPlatformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatorPlatformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorPlatformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
