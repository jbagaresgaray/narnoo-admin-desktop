import { Component, OnInit } from "@angular/core";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import * as _ from "lodash";
import * as async from "async";

import { ByobService } from "../../../services/byob.service";

@Component({
  selector: "app-operator-platforms",
  templateUrl: "./operator-platforms.component.html",
  styleUrls: ["./operator-platforms.component.css"]
})
export class OperatorPlatformsComponent implements OnInit {
  token: string;

  profile: any = {};
  business: any = {};

  showLoading: boolean;

  platformsArr: any[] = [];

  constructor(
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    public byob: ByobService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.showLoading = true;
    this.byob.admin_operator_platforms(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.platformsArr = data.data;
          console.log("platformsArr: ", this.platformsArr);
        }
        this.showLoading = false;
      },
      error => {
        this.showLoading = false;
        console.log("error: ", error);
      }
    );
  }
}
