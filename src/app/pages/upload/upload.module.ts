import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropzoneModule } from "ngx-dropzone-wrapper";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import {
	ButtonsModule,
	BsDropdownModule,
	ModalModule,
	CollapseModule
} from "ngx-bootstrap";

import { UploadComponent } from './upload.component';

@NgModule({
	declarations: [UploadComponent],
	entryComponents: [UploadComponent],
	exports: [UploadComponent],
	imports: [
		CommonModule,
		ComponentsModule,
		PipesModule,
		DropzoneModule,
		ButtonsModule,
		BsDropdownModule,
		ModalModule,
		CollapseModule
	]
})
export class UploadModule { }
