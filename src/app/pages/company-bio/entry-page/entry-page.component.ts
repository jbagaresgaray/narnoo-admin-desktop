import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { QuillEditorComponent } from "ngx-quill";
import Quill from "quill";
import * as _ from "lodash";

import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { BusinessService } from "../../../services/business.service";

@Component({
  selector: "app-entry-page",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./entry-page.component.html",
  styleUrls: ["./entry-page.component.css"]
})
export class EntryPageComponent implements OnInit {
  token: string;
  action: string;
  type: string;

  item: any = {};
  textEditorConfig: any = {};

  isSaving: boolean;
  submitted: boolean;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private business: BusinessService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
  }

  ngOnInit() {
    this.textEditorConfig = {
      toolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ align: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ size: ["small", false, "large", "huge"] }],
        [{ font: [] }]
      ]
    };

    this.isSaving = false;
    this.submitted = false;
    this.type = _.upperFirst(this.type);
  }

  saveChanges() {
    const updateBio = (item: any) => {
      this.toastr.info("Updating...", "INFO");
      this.business.business_biography_edit(this.token, item).then(
        (data: any) => {
          if (data && data.success) {
            console.log("success: ", data);
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            this.bsModalRef.hide();
          } else if (data && !data.success) {
            this.toastr.warning(data.message, "WARNING");
          }
          this.isSaving = false;
        },
        error => {
          console.log("error: ", error);
          this.isSaving = false;
          if (error && !error.success) {
            this.toastr.error(error.error, "ERROR");
          }
        }
      );
    };

    this.coolDialogs
      .confirm("Update Company bio for " + this.type + " ?")
      .subscribe(res => {
        if (res) {
          this.submitted = true;
          this.isSaving = true;

          updateBio(this.item);
        }
      });
  }
}
