import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as _ from "lodash";
import * as $ from "jquery";

import { BusinessService } from "../../services/business.service";

import { EntryPageComponent } from "./entry-page/entry-page.component";
import { CreatePageComponent } from "./create-page/create-page.component";

@Component({
  selector: "app-company-bio",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./company-bio.component.html",
  styleUrls: ["./company-bio.component.css"]
})
export class CompanyBioComponent implements OnInit {
  isSummary = true;
  isDescription = true;

  business: any = {};
  token: string;

  descriptionArr: any[] = [];
  summaryArr: any[] = [];

  showContent: boolean;
  showContentErr: boolean;
  showCheckbox: boolean;
  contentErr: any = {};

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private businessServices: BusinessService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    this.showContent = false;
    this.showContentErr = false;
    this.showCheckbox = false;

    this.initEmptyData();
    this.initData();
  }

  private initEmptyData() {
    this.descriptionArr = [
      {
        sortorder: 1,
        language: "english",
        size: "description",
        text: ""
      },
      {
        sortorder: 2,
        language: "chinese",
        size: "description",
        text: ""
      },
      {
        sortorder: 3,
        language: "japanese",
        size: "description",
        text: ""
      }
    ];
    this.summaryArr = [
      {
        sortorder: 1,
        language: "english",
        size: "summary",
        text: ""
      },
      {
        sortorder: 2,
        language: "chinese",
        size: "summary",
        text: ""
      },
      {
        sortorder: 3,
        language: "japanese",
        size: "summary",
        text: ""
      }
    ];
  }

  initData() {
    this.showContent = false;
    this.businessServices.business_biography(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.descriptionArr = [];
          this.summaryArr = [];
          const biog = data.data;

          let result: any = _.filter(biog, { size: "description" });
          const _english = _.find(result, { language: "english" });
          const _chinese = _.find(result, { language: "chinese" });
          const _japanese = _.find(result, { language: "japanese" });

          if (_.isEmpty(_english)) {
            result.push({
              language: "english",
              size: "description",
              text: ""
            });
          }

          if (_.isEmpty(_chinese)) {
            result.push({
              language: "chinese",
              size: "description",
              text: ""
            });
          }

          if (_.isEmpty(_japanese)) {
            result.push({
              language: "japanese",
              size: "description",
              text: ""
            });
          }

          _.each(result, (row: any) => {
            row.selected = false;
            if (_.lowerCase(row.language) === "english") {
              row.sortorder = 1;
            } else if (_.lowerCase(row.language) === "chinese") {
              row.sortorder = 2;
            } else if (_.lowerCase(row.language) === "japanese") {
              row.sortorder = 3;
            }
          });
          this.descriptionArr = _.orderBy(result, ["sortorder"], ["asc"]);

          let result2: any = _.filter(biog, { size: "summary" });
          console.log("result2: ", result2);
          const _english2 = _.find(result2, { language: "english" });
          const _chinese2 = _.find(result2, { language: "chinese" });
          const _japanese2 = _.find(result2, { language: "japanese" });
          console.log("_chinese2: ",_chinese2);
          console.log("_chinese2: ",_japanese2);

          if (_.isEmpty(_english2)) {
            result2.push({
              language: "english",
              size: "summary",
              text: ""
            });
          }

          if (_.isEmpty(_chinese2)) {
            result2.push({
              language: "chinese",
              size: "summary",
              text: ""
            });
          }

          if (_.isEmpty(_japanese2)) {
            result2.push({
              language: "japanese",
              size: "summary",
              text: ""
            });
          }

          _.each(result2, (row: any) => {
            row.selected = false;
            if (_.lowerCase(row.language) === "english") {
              row.sortorder = 1;
            } else if (_.lowerCase(row.language) === "chinese") {
              row.sortorder = 2;
            } else if (_.lowerCase(row.language) === "japanese") {
              row.sortorder = 3;
            }
          });
          this.summaryArr = _.orderBy(result2, ["sortorder"], ["asc"]);
        }
        this.showContent = true;
        console.log("descriptionArr: ", this.descriptionArr);
        console.log("summaryArr: ", this.summaryArr);
      },
      error => {
        this.showContent = true;
        if (error) {
          this.showContentErr = true;
          this.contentErr = error;
        }
      }
    );
  }

  refresh() {
    this.showContent = true;
    this.showContentErr = false;
    this.initData();
  }

  selectDescription(item: any) {
    _.each(this.descriptionArr, (row: any) => {
      if (item.id === row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  selectSummary(item: any) {
    _.each(this.summaryArr, (row: any) => {
      if (item.id === row.id) {
        row.selected = !item.selected;
      } else {
        row.selected = false;
      }
    });
  }

  createBio(type: string) {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      type: type,
      action: "create"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(CreatePageComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  updateBio(item, type) {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      item: _.cloneDeep(item),
      type: type,
      action: "update"
    };
    console.log("initialState: ", initialState);

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(EntryPageComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
