import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CompanyBioComponent } from "./company-bio.component";

import { QuillModule } from "ngx-quill";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  CollapseModule,
  AccordionModule,
  TabsModule,
  BsDropdownModule
} from "ngx-bootstrap";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { EntryPageComponent } from "./entry-page/entry-page.component";
import { CreatePageComponent } from "./create-page/create-page.component";

@NgModule({
  declarations: [CompanyBioComponent, EntryPageComponent, CreatePageComponent],
  imports: [
    CommonModule,
    CollapseModule,
    AccordionModule,
    ComponentsModule,
    PipesModule,
    TabsModule,
    BsDropdownModule,
    FormsModule,
    QuillModule,
    ReactiveFormsModule
  ],
  entryComponents: [EntryPageComponent, CreatePageComponent]
})
export class CompanyBioModule {}
