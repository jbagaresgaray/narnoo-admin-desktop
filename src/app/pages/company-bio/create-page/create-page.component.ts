import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { QuillEditorComponent } from "ngx-quill";
import Quill from "quill";
import * as _ from "lodash";

import { BusinessService } from "../../../services/business.service";
import { setTime } from "ngx-bootstrap/chronos/utils/date-setters";

@Component({
  selector: "app-create-page",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./create-page.component.html",
  styleUrls: ["./create-page.component.css"]
})
export class CreatePageComponent implements OnInit {
  isSummary = true;
  isDescription = false;
  isSaving = false;

  token: string;
  action: string;
  type: string;

  isSumEnglish: boolean;
  isSumChinese: boolean;
  isSumJapanese: boolean;

  isDescEnglish: boolean;
  isDescChinese: boolean;
  isDescJapanese: boolean;

  item: any = {};

  textEditorConfig: any = {};

  constructor(
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private business: BusinessService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
  }

  ngOnInit() {
    this.isSumEnglish = true;
    this.isSumChinese = false;
    this.isSumJapanese = false;

    this.isDescEnglish = true;
    this.isDescChinese = false;
    this.isDescJapanese = false;

    if (this.type === "summary") {
      this.item.size = "summary";
    } else if (this.type === "description") {
      this.item.size = "description";
    }

    this.type = _.upperFirst(this.type);

    this.textEditorConfig = {
      toolbar: [
        ["bold", "italic", "underline", "strike"],
        [{ align: [] }],
        [{ list: "ordered" }, { list: "bullet" }],
        [{ size: ["small", false, "large", "huge"] }],
        [{ font: [] }]
      ]
    };
  }

  showSummary() {
    this.isSummary = !this.isSummary;
  }

  saveChanges() {
    const updateBio = item => {
      this.isSaving = true;
      this.toastr.info("Updating...", "INFO");
      this.business.business_biography_edit(this.token, item).then(
        (data: any) => {
          if (data && data.success) {
            console.log("success: ", data);
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            setTimeout(() => {
              this.bsModalRef.hide();
            }, 600);
          } else if (data && !data.success) {
            this.toastr.warning(data.data, "WARNING");
          }
          this.isSaving = false;
        },
        error => {
          console.log("error: ", error);
          this.isSaving = false;
          if (error && !error.success) {
            this.toastr.error(error.message, "ERROR");
          }
        }
      );
    };

    this.coolDialogs.confirm("Create Company Bio?").subscribe(res => {
      if (res) {
        console.log("item: ", this.item);
        updateBio(this.item);
      }
    });
  }
}
