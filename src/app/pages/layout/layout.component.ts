import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { BroadcasterService } from "ng-broadcaster";
import { ConsoleService } from "@ng-select/ng-select/ng-select/console.service";

declare let $: any;
declare var jQuery: any;

@Component({
  selector: "app-layout",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.css"]
})
export class LayoutComponent implements OnInit {
  isMinimized = true;
  constructor(private broadcaster: BroadcasterService) {
    this.broadcaster.on<any>("js-page-sidebar").subscribe(isMinimized => {
      this.isMinimized = isMinimized;
      console.log("this.isMinimized: ", this.isMinimized);
    });
  }

  ngOnInit() {
    // const $document = $(document),
    //   $sidebar = $(".js-page-sidebar"), // page sidebar itself
    //   $sidebarToggle = $(".c-sidebar-toggle"), // sidebar toggle icon
    //   $sidebarToggleContainer = $(".c-navbar"), // component that contains sidebar toggle icon
    //   $sidebarItem = $(".c-sidebar__item"),
    //   $sidebarSubMenu = $(".c-sidebar__submenu");
    // $sidebarToggleContainer.on("click", function(e) {
    //   const $target = $(e.target);
    //   if ($target.closest($sidebarToggle).length) {
    //     $sidebar.addClass("is-visible");
    //     return false;
    //   }
    // });
    // // Bootstrap collapse.js plugin is used for handling sidebar submenu.
    // $sidebarSubMenu.on("show.bs.collapse", function() {
    //   $(this)
    //     .parent($sidebarItem)
    //     .addClass("is-open");
    // });
    // $sidebarSubMenu.on("hide.bs.collapse", function() {
    //   $(this)
    //     .parent($sidebarItem)
    //     .removeClass("is-open");
    // });
    // $sidebarItem.on("click", function(e) {
    //   $sidebar.removeClass("is-visible");
    // });
  }
}
