import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import * as $ from "jquery";

@Component({
  selector: "app-forgot",
  templateUrl: "./forgot.component.html",
  styleUrls: ["./forgot.component.css"]
})
export class ForgotComponent implements OnInit {
  constructor(public router: Router) {}

  ngOnInit() {
    $("body").addClass("o-page");
    $("body").addClass("o-page--center");
  }

  registerUser() {
    this.router.navigate(["/register"]);
  }
}
