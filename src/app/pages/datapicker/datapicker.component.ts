import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  NgZone,
  ViewEncapsulation
} from "@angular/core";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import * as $ from "jquery";
import * as _ from "lodash";
import * as async from "async";

import { ImagesService } from "../../services/images.service";
import { LogosService } from "../../services/logos.service";
import { PrintService } from "../../services/print.service";
import { VideosService } from "../../services/videos.service";
import { AlbumsService } from "../../services/albums.service";
import { ProductsService } from "../../services/products.service";

import { CollectionsService } from "../../services/collections.service";
import { ChannelsService } from "../../services/channels.service";
import { RouterLinkWithHref } from "@angular/router";

@Component({
  selector: "app-datapicker",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./datapicker.component.html",
  styleUrls: ["./datapicker.component.css"]
})
export class DatapickerComponent implements OnInit {
  action: string;
  title: string;
  token: string;
  params: any = {};
  business: any = {};

  perPage = 16;
  showLoading: boolean = false;
  isSaving: boolean = false;

  showContentImage: boolean = false;
  showContentImageErr: boolean = false;
  imageContentErr: any = {};
  pageImage: number = 1;
  totalPageImage: number = 0;
  totalImageCount: number = 0;

  showContentLogo: boolean = false;
  showContentLogoErr: boolean = false;
  logoContentErr: any = {};
  pageLogo: number = 1;
  totalPageLogo: number = 0;
  totalLogoCount: number = 0;

  showContentPrint: boolean = false;
  showContentPrintErr: boolean = false;
  printContentErr: any = {};
  pagePrint: number = 1;
  totalPagePrint: number = 0;
  totalPrintCount: number = 0;

  showContentVideos: boolean = false;
  showContentVideosErr: boolean = false;
  videosContentErr: any = {};
  pageVideo: number = 1;
  totalPageVideo: number = 0;
  totalVideoCount: number = 0;

  showContentAlbums: boolean = false;
  showContentAlbumsErr: boolean = false;
  albumsContentErr: any = {};
  pageAlbum: number = 1;
  totalPageAlbum: number = 0;
  totalAlbumCount: number = 0;

  imagesLoadingArr: any[] = [];
  selectedMedia: any[] = [];

  imagesArr: any[] = [];
  logosArr: any[] = [];
  printArr: any[] = [];
  videosArr: any[] = [];
  albumsArr: any[] = [];

  constructor(
    public bsModalRef: BsModalRef,
    private changeDetection: ChangeDetectorRef,
    public zone: NgZone,
    public toastr: ToastrService,
    public coolDialog: NgxCoolDialogsService,
    private modalService: BsModalService,
    private images: ImagesService,
    private logos: LogosService,
    private prints: PrintService,
    private videos: VideosService,
    private collections: CollectionsService,
    private channels: ChannelsService,
    private albums: AlbumsService,
    private products: ProductsService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("business")) || {};
    for (let i = 0; i < 16; ++i) {
      this.imagesLoadingArr.push(i);
    }
  }

  ngOnInit() {
    this.showLoading = true;
    console.log("this.action: ", this.action);
    console.log("this.params: ", this.params);

    if (
      this.action === "image" ||
      this.action === "feature_image" ||
      this.action === "gallery"
    ) {
      this.showContentImage = false;
      this.showContentImageErr = false;

      this.loadImage(this.pageImage, this.perPage);
    } else if (this.action === "logo" || this.action === "feature_logo") {
      this.showContentLogo = false;
      this.showContentLogoErr = false;

      this.loadLogos(this.pageLogo, this.perPage);
    } else if (this.action === "print" || this.action === "feature_print") {
      this.showContentPrint = false;
      this.showContentPrintErr = false;

      this.loadPrints(this.pagePrint, this.perPage);
    } else if (this.action === "video" || this.action === "feature_video") {
      this.showContentVideos = false;
      this.showContentVideosErr = false;

      this.loadVideos(this.pageVideo, this.perPage);
    } else if (this.action === "albums") {
      this.showContentAlbums = false;
      this.showContentAlbumsErr = false;

      this.loadAlbums(this.pageAlbum, this.perPage);
    }
  }

  private loadImage(page, total) {
    this.images
      .image_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.imagesArr = [];

            this.totalPageImage = parseInt(data.data.totalPages, 0);
            this.totalImageCount = parseInt(data.data.count, 0);

            for (let i = 0; i < _.size(data.data.images); i++) {
              data.data.images[i].selected = false;

              if (_.isEmpty(data.data.images[i].caption)) {
                data.data.images[i].caption = "No Caption";
              }
              if (_.isEmpty(data.data.images[i].location)) {
                data.data.images[i].location = "No Location";
              }

              this.imagesArr.push(data.data.images[i]);
            }

            if (!_.isEmpty(this.selectedMedia)) {
              _.each(this.imagesArr, (row: any) => {
                const result = _.find(this.selectedMedia, { id: row.id });
                if (result) {
                  row.selected = true;
                }
              });
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentImageErr = true;
            this.imageContentErr = {
              message: "No results found"
            };
          }
          this.showContentImage = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentImage = true;
          this.showContentImageErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.imageContentErr = error;
          }
        }
      );
  }

  private loadLogos(page, total) {
    this.logos
      .logos_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.logosArr = [];
            this.totalPageLogo = parseInt(data.data.totalPages, 0);
            this.totalLogoCount = parseInt(data.data.count, 0);

            for (let i = 0; i < _.size(data.data.logos); i++) {
              data.data.logos[i].selected = false;
              this.logosArr.push(data.data.logos[i]);
            }

            if (!_.isEmpty(this.selectedMedia)) {
              _.each(this.logosArr, (row: any) => {
                const result = _.find(this.selectedMedia, { id: row.id });
                if (result) {
                  row.selected = true;
                }
              });
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentLogoErr = true;
            this.logoContentErr = {
              message: "No results found"
            };
          }
          this.showContentLogo = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        error => {
          this.showContentLogo = true;
          this.showContentLogoErr = true;
          this.showLoading = false;

          if (error && !error.success) {
            this.logoContentErr = error;
          }
        }
      );
  }

  private loadPrints(page, total) {
    this.prints
      .brochure_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.printArr = [];
            this.totalPagePrint = parseInt(data.data.totalPages, 0);
            this.totalPrintCount = parseInt(data.data.count, 0);

            for (let i = 0; i < _.size(data.data.prints); i++) {
              data.data.prints[i].selected = false;
              if (_.isEmpty(data.data.prints[i].brochure_caption)) {
                data.data.prints[i].brochure_caption = "No caption";
              }
              this.printArr.push(data.data.prints[i]);
            }
            if (!_.isEmpty(this.selectedMedia)) {
              _.each(this.printArr, (row: any) => {
                const result = _.find(this.selectedMedia, { id: row.id });
                if (result) {
                  row.selected = true;
                }
              });
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentPrintErr = true;
            this.printContentErr = {
              message: "No results found"
            };
          }

          this.showContentPrint = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        (error: any) => {
          this.showContentPrint = true;
          this.showContentPrintErr = true;

          if (error && !error.success) {
            this.printContentErr = error;
          }
        }
      );
  }

  private loadVideos(page, total) {
    this.videos
      .video_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.videosArr = [];
            this.totalPageVideo = parseInt(data.data.totalPages, 0);
            this.totalVideoCount = parseInt(data.data.count, 0);

            for (let i = 0; i < _.size(data.data.videos); i++) {
              data.data.videos[i].selected = false;
              if (_.isEmpty(data.data.videos[i].caption)) {
                data.data.videos[i].caption = "No caption";
              }
              this.videosArr.push(data.data.videos[i]);
            }
            if (!_.isEmpty(this.selectedMedia)) {
              _.each(this.videosArr, (row: any) => {
                const result = _.find(this.selectedMedia, { id: row.id });
                if (result) {
                  row.selected = true;
                }
              });
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentVideosErr = true;
            this.videosContentErr = {
              message: "No results found"
            };
          }
          this.showContentVideos = true;
          this.showLoading = false;

          setTimeout(() => {
            $(".progressive-image").each(function() {
              const image = new Image();
              const previewImage = $(this).find(".loadingImage");
              const newImage = $(this).find(".overlay");
              image.src = previewImage.data("image");
              image.onload = function() {
                newImage.css("background-image", "url(" + image.src + ")");
                newImage.css("opacity", "1");
              };
              image.onerror = function() {
                newImage.css(
                  "background-image",
                  "url(../assets/img/thumb.jpg)"
                );
                newImage.css("opacity", "1");
              };
            });
          }, 1000);
        },
        error => {
          this.showContentVideos = true;
          this.showContentVideosErr = true;
          this.showLoading = false;
          if (error && !error.success) {
            this.videosContentErr = error;
          }
        }
      );
  }

  private loadAlbums(page, total, refresher?: any, infinite?: any) {
    this.albums
      .album_list(this.token, {
        page: page,
        total: total
      })
      .then(
        (data: any) => {
          if (data && data.success && data.data[0] !== false) {
            this.albumsArr = [];
            this.totalPageAlbum = parseInt(data.data.totalPages, 0);
            this.totalAlbumCount = parseInt(data.data.count, 0);

            for (let i = 0; i < _.size(data.data.albums); i++) {
              data.data.albums[i].selected = false;
              if (_.isEmpty(data.data.albums[i].title)) {
                data.data.albums[i].title = "No caption";
              }
              this.albumsArr.push(data.data.albums[i]);
            }
          } else if (data && data.success && data.data[0] === false) {
            this.showContentAlbumsErr = true;
            this.albumsContentErr = {
              message: "No results found"
            };
          }
          this.showContentAlbums = true;
          if (refresher) {
            refresher.complete();
          }
          if (infinite) {
            infinite.complete();
          }
        },
        error => {
          this.showContentAlbums = true;
          this.showContentAlbumsErr = true;
          if (refresher) {
            refresher.complete();
          }
          if (infinite) {
            infinite.complete();
          }
          if (error && !error.success) {
            this.albumsContentErr = error;
          }
        }
      );
  }

  selectMedia(item: any) {
    if (this.action === "feature_image") {
      _.each(this.imagesArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        } else {
          row.selected = true;
        }
      });
    } else if (this.action === "feature_logo") {
      _.each(this.logosArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        } else {
          row.selected = true;
        }
      });
    } else if (this.action === "feature_print") {
      item.selected = !item.selected;
      _.each(this.printArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        } else {
          row.selected = true;
        }
      });
    } else if (this.action === "feature_video") {
      item.selected = !item.selected;
      _.each(this.videosArr, (row: any) => {
        if (row.id !== item.id) {
          row.selected = false;
        } else {
          row.selected = true;
        }
      });
    } else {
      if (_.find(this.selectedMedia, { id: item.id })) {
        item.selected = false;
        _.remove(this.selectedMedia, (row: any) => {
          return row.id === item.id;
        });
      } else {
        item.selected = true;
        this.selectedMedia.push(item);
      }
    }
  }

  viewMore(event: any) {
    console.log("Page changed to: " + event.page);
    console.log("Number items per page: " + event.itemsPerPage);

    if (
      this.action === "image" ||
      this.action === "feature_image" ||
      this.action === "gallery"
    ) {
      this.pageImage = event.page;
      this.showLoading = true;
      this.showContentImage = false;
      this.showContentImageErr = false;

      this.loadImage(this.pageImage, this.perPage);
    } else if (this.action === "logo") {
      this.pageLogo = event.page;
      this.showLoading = true;
      this.showContentLogo = false;
      this.showContentLogoErr = false;

      this.loadLogos(this.pageLogo, this.perPage);
    } else if (this.action === "print") {
      this.pagePrint = event.page;
      this.showLoading = true;
      this.showContentPrint = false;
      this.showContentPrintErr = false;

      this.loadPrints(this.pagePrint, this.perPage);
    } else if (this.action === "video") {
      this.pageVideo = event.page;
      this.showLoading = true;
      this.loadVideos(this.pageVideo, this.perPage);
    } else if (this.action === "albums") {
      this.pageAlbum = event.page;
      this.showLoading = true;
      this.showContentVideos = false;
      this.showContentVideosErr = false;

      this.loadLogos(this.pageAlbum, this.perPage);
    }
  }

  saveChanges() {
    if (this.action === "image") {
    } else if (
      this.action === "feature_image" ||
      this.action === "feature_video" ||
      this.action === "feature_print" ||
      this.action === "feature_logo"
    ) {
      this.featureUtilsItems(this.action);
    } else if (this.action === "gallery") {
      this.addImagesToProduct();
    } else if (this.action === "logo") {
    } else if (this.action === "print") {
    } else if (this.action === "video") {
    } else if (this.action === "albums") {
    }
  }

  private featureUtilsItems(action) {
    let selectedDatas: any = {};
    let _action: string;

    if (action === "feature_image") {
      selectedDatas = _.find(this.imagesArr, (row: any) => {
        return row.selected === true;
      });
      _action = "image";
    } else if (action === "feature_video") {
      selectedDatas = _.find(this.videosArr, (row: any) => {
        return row.selected === true;
      });
      _action = "video";
    } else if (action === "feature_print") {
      selectedDatas = _.find(this.printArr, (row: any) => {
        return row.selected === true;
      });
      _action = "print";
    } else if (action === "feature_logo") {
      selectedDatas = _.find(this.logosArr, (row: any) => {
        return row.selected === true;
      });
      _action = "logo";
    }

    if (_.isEmpty(selectedDatas)) {
      this.toastr.warning("Please select image to feature!", "WARNING");
      return;
    }
    console.log("selectedDatas: ", selectedDatas);
    this.coolDialog
      .confirm("Are you sure to add the selected " + _action + "?")
      .subscribe(res => {
        if (res) {
          this.featureProductItem(_action, selectedDatas.id);
        }
      });
  }

  private featureProductItem(type, mediaId) {
    this.toastr.info("Featuring...", "INFO");
    this.isSaving = true;
    this.products
      .product_set_feature_item(this.token, {
        productId: this.params.productId,
        mediaId: mediaId,
        type: type
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            console.log("success: ", data);
            this.toastr.success(data.data, "SUCCESS");
            this.modalService.setDismissReason("save");
            setTimeout(() => {
              this.bsModalRef.hide();
            }, 300);
          } else if (data && !data.success) {
            this.toastr.warning(data.message, "WARNING");
          }
          this.isSaving = false;
        },
        (error: any) => {
          console.log("error: ", error);
          this.isSaving = false;
          if (error && !error.success) {
            this.toastr.error(error.message, "ERROR");
          }
        }
      );
  }

  private addImagesToProduct() {
    console.log("this.selectedMedia: ", this.selectedMedia);

    let selectedDatas = _.filter(this.selectedMedia, (row: any) => {
      return row.selected === true;
    });
    selectedDatas = _.map(selectedDatas, (row: any) => {
      return row.id;
    });
    if (_.isEmpty(selectedDatas)) {
      this.toastr.warning(
        "Unable to upload when no images are selected!",
        "WARNING"
      );
      return;
    }

    const addImagesToProduct = () => {
      this.toastr.info("Featuring...", "INFO");
      this.isSaving = true;
      this.products
        .product_add_image(this.token, {
          productId: this.params.productId,
          image: selectedDatas
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              this.toastr.success(data.data, "SUCCESS");
              this.modalService.setDismissReason("save");
              setTimeout(() => {
                this.bsModalRef.hide();
              }, 300);
            } else if (data && !data.success) {
              this.toastr.warning(data.message, "WARNING");
            }
            this.isSaving = false;
          },
          error => {
            console.log("error: ", error);
            this.isSaving = false;
            if (error && !error.success) {
              this.toastr.error(error.message, "ERROR");
            }
          }
        );
    };

    this.coolDialog
      .confirm("Are you sure to add the selected images as to product gallery?")
      .subscribe(res => {
        if (res) {
          addImagesToProduct();
        }
      });
  }
}
