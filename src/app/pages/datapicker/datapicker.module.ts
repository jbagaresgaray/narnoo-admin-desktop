import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";
import { BsDropdownModule, PaginationModule } from "ngx-bootstrap";
import { FormsModule } from "@angular/forms";

import { DatapickerComponent } from "./datapicker.component";

@NgModule({
  declarations: [DatapickerComponent],
  entryComponents: [DatapickerComponent],
  imports: [
    CommonModule,
    BsDropdownModule,
    PaginationModule,
    ComponentsModule,
    PipesModule,
    FormsModule
  ]
})
export class DatapickerModule {}
