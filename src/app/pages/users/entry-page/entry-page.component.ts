import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { UsersService } from "../../../services/users.service";

@Component({
  selector: "app-entry-page",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./entry-page.component.html",
  styleUrls: ["./entry-page.component.css"]
})
export class EntryPageComponent implements OnInit {
  token: string;
  item: any = {};
  isSaving: boolean;
  submitted: boolean;
  action: string;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private users: UsersService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
  }

  ngOnInit() {
    this.isSaving = false;
    this.submitted = false;
    console.log("item: ", this.item);

    if (this.action === "create") {
      this.entryForm = this.formBuilder.group({
        email: ["", Validators.required],
        role: ["", Validators.required]
      });
    } else {
      this.entryForm = this.formBuilder.group({
        id: ["", Validators.required],
        role: ["", Validators.required]
      });
    }

    this.entryForm.patchValue(this.item);
  }

  saveChanges() {
    this.submitted = true;
    this.toggleFormState();

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      return;
    }

    this.coolDialogs.confirm("Save User Entry?").subscribe(res => {
      if (res) {
        if (this.action === "create") {
          this.createUser();
        } else if (this.action === "update") {
          this.updateUser();
        }
      }
    });
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private createUser() {
    this.toastr.info("Creating...", "INFO");
    this.isSaving = true;
    this.users.add_user(this.token, this.entryForm.value).then(
      (data: any) => {
        if (data && data.success) {
          this.toastr.success(data.data, "SUCCESS");
          this.modalService.setDismissReason("save");
          this.bsModalRef.hide();
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
        this.isSaving = false;
      },
      error => {
        console.log("error: ", error);
        this.isSaving = false;
        if (error && !error.success) {
          this.toastr.error(error.message, "ERROR");
        }
      }
    );
  }

  private updateUser() {
    this.toastr.info("Updating...", "INFO");
    this.isSaving = true;
    if (this.item.role === "Administrator") {
      this.item.role = 1;
    } else if (this.item.role === "Staff") {
      this.item.role = 2;
    } else if (this.item.role === "Trade") {
      this.item.role = 3;
    } else if (this.item.role === "Media") {
      this.item.role = 4;
    }
    this.users.user_edit_access(this.token, this.entryForm.value).then(
      (data: any) => {
        if (data && data.success) {
          this.toastr.success(data.data, "SUCCESS");
          this.modalService.setDismissReason("save");
          this.bsModalRef.hide();
        } else {
          this.toastr.warning(data.message, "WARNING");
        }
        this.isSaving = false;
      },
      error => {
        console.log("error: ", error);
        this.isSaving = false;
        if (error && !error.success) {
          this.toastr.error(error.message, "ERROR");
        }
      }
    );
  }
}
