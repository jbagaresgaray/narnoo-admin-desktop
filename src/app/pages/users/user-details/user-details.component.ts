import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { UsersService } from "../../../services/users.service";
import { UtilitiesService } from "../../../services/utilities.service";

import { EntryPageComponent } from "../entry-page/entry-page.component";

@Component({
  selector: "app-user-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.css"]
})
export class UserDetailsComponent implements OnInit {
  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];

  user: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private users: UsersService,
    private utilities: UtilitiesService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      console.log("params: ", params); // {order: "popular"}
      if (params.user) {
        this.user = JSON.parse(params.user);
      }
    });
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  updateUser() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.router.navigate(["/users"]);
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update",
      item: this.user
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(EntryPageComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
