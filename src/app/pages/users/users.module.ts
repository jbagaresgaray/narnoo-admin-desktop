import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ModalModule, BsDropdownModule } from "ngx-bootstrap";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

import { UsersComponent } from "./users.component";
import { EntryPageComponent } from "./entry-page/entry-page.component";
import { UserDetailsComponent } from "./user-details/user-details.component";

@NgModule({
  declarations: [UsersComponent, EntryPageComponent, UserDetailsComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule,
    BsDropdownModule,
    ComponentsModule,
    PipesModule,
    ReactiveFormsModule
  ],
  entryComponents: [EntryPageComponent]
})
export class UsersModule {}
