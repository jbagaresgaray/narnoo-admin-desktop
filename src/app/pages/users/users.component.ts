import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import * as CryptoJS from "crypto-js";
import * as _ from "lodash";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";

import { UsersService } from "../../services/users.service";
import { UtilitiesService } from "../../services/utilities.service";

import { EntryPageComponent } from "./entry-page/entry-page.component";

@Component({
  selector: "app-users",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.css"]
})
export class UsersComponent implements OnInit {
  contactsArr: any[] = [];
  contactsArrCopy: any[] = [];
  fakeArr: any[] = [];

  token: string;
  showContent: boolean;
  business: any = {};

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    private modalService: BsModalService,
    private users: UsersService,
    private utilities: UtilitiesService
  ) {
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    this.token = localStorage.getItem("admin.bus.token");
    for (let i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.initializeData();
  }

  initializeData() {
    this.showContent = false;
    this.users.user_list(this.token).then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.data, (row: any) => {
            row.gravatar =
              "https://www.gravatar.com/avatar/" +
              CryptoJS.MD5(row.email.toLowerCase(), "hex");
          });
          this.contactsArr = data.data;
          this.contactsArrCopy = _.cloneDeep(data.data);
        }
        this.showContent = true;
      },
      error => {
        this.showContent = true;
      }
    );
  }

  errorAvatarHandler(event) {
    event.target.src = this.utilities.radomizeAvatar();
  }

  revokeUser(item) {
    if (this.business.role === 3 || this.business.role === 4) {
      this.toastr.warning("Invalid Access!!!", "WARNING");
      return;
    }

    this.coolDialogs
      .confirm("Are you sure to delete this User?")
      .subscribe(res => {
        if (res) {
          this.users.revoke_access(this.token, item).then((data: any) => {
            if (data && data.success) {
              this.toastr.success(data.data, "SUCCESS");
              setTimeout(() => {
                this.initializeData();
              }, 300);
            } else {
              this.toastr.warning(data.message, "WARNING");
            }
          });
        }
      });
  }

  createUser() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "create"
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(EntryPageComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  gotoDetail(item) {
    console.log("item: ", item);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        userId: item.id,
        user: JSON.stringify(item)
      },
      queryParamsHandling: "merge"
    };
    this.router.navigate(["users-detail"], navigationExtras);
  }

  updateUser(item) {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initializeData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      action: "update",
      item: _.cloneDeep(item)
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-md",
      initialState
    };

    this.bsModalRef = this.modalService.show(EntryPageComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
