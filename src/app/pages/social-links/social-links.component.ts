import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef
} from "@angular/core";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { combineLatest, Subscription } from "rxjs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { BusinessService } from "../../services/business.service";

import { EntryPageComponent } from "./entry-page/entry-page.component";

@Component({
  selector: "app-social-links",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./social-links.component.html",
  styleUrls: ["./social-links.component.css"]
})
export class SocialLinksComponent implements OnInit {
  token: string;
  link: any = {};
  fakeArr: any[] = [];

  showLoading = true;
  showEdit = false;
  showContentErr = false;
  contentErr: any = {};
  business: any = {};

  bsModalRef: BsModalRef;
  subscriptions: Subscription[] = [];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private changeDetection: ChangeDetectorRef,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private businessServices: BusinessService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};

    for (let i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }
  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.showLoading = true;
    this.showContentErr = false;
    this.businessServices.business_social_links(this.token).then(
      (data: any) => {
        if (data && data.success) {
          this.link = data.data;
        }
        this.showLoading = false;
        this.showContentErr = false;
      },
      error => {
        this.showLoading = false;
        if (error) {
          this.showContentErr = true;
          this.contentErr = error;
          console.log("this.contentErr: ", this.contentErr);
        }
      }
    );
  }

  editSocialLinks() {
    const _combine = combineLatest(
      this.modalService.onHide,
      this.modalService.onHidden
    ).subscribe(() => this.changeDetection.markForCheck());

    this.subscriptions.push(
      this.modalService.onHide.subscribe((reason: string) => {
        console.log("onHide callbackResponse: ", reason);
        if (reason === "save") {
          this.initData();
        }
      })
    );
    this.subscriptions.push(
      this.modalService.onHidden.subscribe((reason: string) => {
        console.log("onHidden callbackResponse: ", reason);
        this.unsubscribe();
      })
    );
    this.subscriptions.push(_combine);

    const initialState: any = {
      link: this.link
    };

    const modalConfig: any = {
      animated: true,
      class: "modal-lg",
      initialState
    };

    this.bsModalRef = this.modalService.show(EntryPageComponent, modalConfig);
    this.bsModalRef.content.closeBtnName = "Close";
  }

  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
}
