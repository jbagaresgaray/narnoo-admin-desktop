import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

import { BusinessService } from "../../../services/business.service";

@Component({
  selector: "app-entry-page",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./entry-page.component.html",
  styleUrls: ["./entry-page.component.css"]
})
export class EntryPageComponent implements OnInit {
  token: string;
  business: any = {};
  link: any = {};
  isSaving: boolean;
  submitted: boolean;

  entryForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private businessServices: BusinessService,
    private toastr: ToastrService,
    private coolDialogs: NgxCoolDialogsService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService
  ) {
    this.token = localStorage.getItem("admin.bus.token");
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
  }

  ngOnInit() {
    console.log("this.link: ", this.link);
    this.isSaving = false;
    this.submitted = false;
    this.link.type = "links";

    this.entryForm = this.formBuilder.group({
      type: ["", Validators.required],
      tripadvisor: ["", Validators.required],
      facebook: ["", Validators.required],
      twitter: ["", Validators.required],
      instagram: ["", Validators.required],
      youtube: ["", Validators.required],
      vimeo: ["", Validators.required],
      pinterest: ["", Validators.required]
    });

    if(this.link){
      this.entryForm.patchValue(this.link);
    }
  }

  get f(): any {
    return this.entryForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  private toggleFormState() {
    const state = this.isSaving ? "disable" : "enable";
    Object.keys(this.entryForm.controls).forEach(controlName => {
      this.entryForm.controls[controlName][state](); // disables/enables each form control based on 'this.formDisabled'
    });
  }

  saveChanges() {
    this.submitted = true;
    this.toggleFormState();

    if (this.entryForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.entryForm);
      this.toggleFormState();
      return;
    }

    this.coolDialogs.confirm("Update Social links?").subscribe(res => {
      if (res) {
        this.toastr.info("Updating...", "INFO");
        this.isSaving = true;

        console.log("this.link: ", this.link);

        this.businessServices
          .business_social_links_edit(this.token, this.entryForm.value)
          .then(
            (data: any) => {
              if (data && data.success) {
                this.toastr.success(data.message, "SUCCESS");
                this.modalService.setDismissReason("save");
                this.bsModalRef.hide();
              } else if (data && !data.success) {
                this.toastr.warning(data.message, "WARNING");
              }
              this.isSaving = false;
            },
            error => {
              this.isSaving = false;
              if (error && !error.success) {
                this.toastr.error(error.message, "ERROR");
              }
            }
          );
      }
    });
  }
}
