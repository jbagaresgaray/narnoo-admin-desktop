import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ModalModule } from "ngx-bootstrap";
import { ClipboardModule } from "ngx-clipboard";

import { SocialLinksComponent } from "./social-links.component";
import { EntryPageComponent } from "./entry-page/entry-page.component";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [SocialLinksComponent, EntryPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ModalModule,
    ClipboardModule,
    ComponentsModule,
    PipesModule,
    ReactiveFormsModule
  ],
  entryComponents: [EntryPageComponent]
})
export class SocialLinksModule {}
