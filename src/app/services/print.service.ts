import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class PrintService {
  constructor(
    public http: HttpClient,
    public restangular: Restangular,
    public toastr: ToastrService
  ) {}

  brochure_list(token, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/list")
        .customGET("", params)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_detail(token, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/details/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_download(token, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/download/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  edit_print(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json"
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_operator_list(token, optId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/list/operator/" + optId)
        .customGET("", params)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_operator_detail(token, Id, optId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/details/" + Id + "/operator/" + optId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_operator_download(token, optId, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/download_operator/" + optId + "/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_distributor_list(token, distId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/list/distributor/" + distId)
        .customGET("", params)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  brochure_distributor_detail(token, Id, distId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/details/" + Id + "/distributor/" + distId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  print_delete(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("brochure/delete")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  share_url_print(token, Id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("share/url/print/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
