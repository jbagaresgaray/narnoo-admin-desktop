import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ByobService {
  constructor(
    public http: HttpClient,
    public restangular: Restangular,
    public toastr: ToastrService
  ) {}

  assigned_platform(Id, token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/assigned_platform/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  platform_options(Id, token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/platform_options/" + Id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  platform_edit(data: any, token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/platform_edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  admin_reservation_platforms(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/platforms")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  admin_map_platforms(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/map_operator")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  admin_map_operator_preference(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/map_operator_preference")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  admin_mapping_product(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/map_product")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  admin_operator_platforms(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/operator_platforms")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  admin_product_platforms(token, productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("byob/product_platforms/" + productId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
