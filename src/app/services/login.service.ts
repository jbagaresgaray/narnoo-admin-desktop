import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  // base_url: string = environment.api_url + environment.api_admin_version;
  constructor(
    public http: HttpClient,
    private restangular: Restangular,
    public toastr: ToastrService
  ) {}

  isLoggedIn = false;

  authenticate(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        this.isLoggedIn = true;
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const headers: any = {
        "API-KEY": environment.api_key,
        "API-SECRET-KEY": environment.api_secret_key,
        "Content-Type": undefined
      };
      const formData = new FormData();
      formData.append("username", data.username);
      formData.append("password", data.password);

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(headers);
        })
        .all("login/authenticate")
        .customPOST(formData, undefined, undefined)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  authenticate_social(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const headers = new HttpHeaders({
        "API-KEY": environment.api_key,
        "API-SECRET-KEY": environment.api_secret_key,
        "Content-type": "application/json"
      });

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(headers);
        })
        .all("login/social")
        .customPOST({
          platform: data.platform,
          id: data.id,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          token: data.token || ""
        })
        .subscribe(callbackResponse, errorResponse);
    });
  }

  forgot_password(email: string) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const headers: any = {
        "API-KEY": environment.api_key,
        "API-SECRET-KEY": environment.api_secret_key
      };
      const formData = new FormData();
      formData.append("email", email);

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(headers);
        })
        .all("login/forgot_password")
        .customPOST(formData, undefined, undefined)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  logout() {
    localStorage.removeItem("app.adminData");
    localStorage.removeItem("app.admintoken");
    localStorage.removeItem("admin.bus.token");
    localStorage.removeItem("admin.business");
  }

  token_validate(token: string) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("token/validate")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
