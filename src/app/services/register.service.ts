import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ToastrService } from "ngx-toastr";
import { Restangular } from "ngx-restangular";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class RegisterService {
  base_url: string = environment.api_url + environment.api_version;
  constructor(
    public http: HttpClient,
    public toastr: ToastrService,
    public restangular: Restangular
  ) {}

  user(data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header = {
        "API-KEY": environment.api_key,
        "API-SECRET-KEY": environment.api_secret_key,
        "Content-Type": "application/json"
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("register/user")
        .customPOST({
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          password: data.password,
          confirmation: data.confirmation
        })
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
