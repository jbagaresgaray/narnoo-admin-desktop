import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  // base_url: string = environment.api_url + environment.api_version;
  constructor(
    public http: HttpClient,
    public restangular: Restangular,
    public toastr: ToastrService
  ) {}

  // API - Products
  product_list(token, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/list")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  // API - Product Details
  product_details(token, prodId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/details/" + prodId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  // API - Product Gallery
  product_gallery(token, prodId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/gallery/" + prodId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  // API - OP --> OP Products
  operator_products(token, businessId, params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/list/" + businessId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  operator_products_detail(token, businessId, prodId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/details/" + prodId + "/" + businessId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  // API - Product Add Image
  product_add_image(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/add_image")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  // API - Product Remove Image
  product_remove_image(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/remove_image")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  // API - Product Set Feature Item
  product_set_feature_item(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/feature")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  //  API CREATE PRODUCT
  product_create(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/create")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  //  API EDIT PRODUCT
  product_edit(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status === 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  //  API EDIT PRODUCT
  product_delete(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/delete")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  //  API EDIT PRODUCT FEATURES
  product_edit_features(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/edit_features")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  //  API EDIT PRODUCT TITLE
  product_edit_title(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        "Content-Type": "application/json;",
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/edit_title")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  product_import_list(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/import_list")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  product_import_details(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/import_details/" + data.platform + "/" + data.code)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  product_import_data(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("product/import_data/" + data.platform + "/" + data.code)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
