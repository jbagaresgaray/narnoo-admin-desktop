import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Subject } from "rxjs/Subject";
import { filter, map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { Restangular } from "ngx-restangular";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class UtilitiesService {
  base_url: string = environment.api_url + environment.api_version;

  public latitude: any;
  public longitude: any;
  public contextMenu: any[] = [];
  public _contextMenu: any[] = [];
  public selectedItem: any = {};
  public selectedData: any[] = [];

  constructor(
    public http: HttpClient,
    public toastr: ToastrService,
    public restangular: Restangular
  ) {}

  radomizeAvatar() {
    const avatarArr = [
      "./assets/avatar/coconut/SVG/coconut-placeholder.svg",
      "./assets/avatar/feet/SVG/feet-placeholder.svg",
      "./assets/avatar/flippers/SVG/flipper-placeholder.svg",
      "./assets/avatar/mask/SVG/mask-placeholder.svg",
      "./assets/avatar/tree/SVG/tree-placeholder.svg"
    ];

    const randomNumber = Math.floor(Math.random() * avatarArr.length);
    return avatarArr[randomNumber];
  }

  category() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + localStorage.getItem("app.admintoken")
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
          config.setBaseUrl(this.base_url);
        })
        .all("utilities/category")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  subcategory(id) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + localStorage.getItem("app.admintoken")
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
          config.setBaseUrl(this.base_url);
        })
        .all("utilities/subcategory/" + id)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  booking_platforms() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + localStorage.getItem("app.admintoken")
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
          config.setBaseUrl(this.base_url);
        })
        .all("utilities/booking_platforms")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  distributor_category() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + localStorage.getItem("app.admintoken")
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
          config.setBaseUrl(this.base_url);
        })
        .all("utilities/distributor_category")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getApppVersion() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      this.http
        .get("../../../package.json")
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getCountries() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + localStorage.getItem("app.admintoken")
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
          config.setBaseUrl(this.base_url);
        })
        .all("utilities/country_names")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
