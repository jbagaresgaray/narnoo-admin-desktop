import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Restangular } from "ngx-restangular";
import { ToastrService } from "ngx-toastr";

import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class BusinessService {
  constructor(
    public http: HttpClient,
    public restangular: Restangular,
    public toastr: ToastrService
  ) {}

  business_profile(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("business/profile")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  business_profile_edit(token, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("business/edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  business_social_links(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("social/links")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  business_social_links_edit(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("social/edit")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  business_biography(token) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("biography/text")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  business_biography_edit(token, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("biography/edit")
        .customPOST(JSON.stringify(data))
        .subscribe(callbackResponse, errorResponse);
    });
  }

  create_business(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + localStorage.getItem("app.admintoken")
      };

      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("business/register")
        .customPOST(JSON.stringify(data))
        .subscribe(callbackResponse, errorResponse);
    });
  }

  business_biography_operator(token, optId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        if (error && error.status == 500) {
          this.toastr.error(error.statusText + ". " + "Try Again!", "ERROR");
        }
        reject(error.error);
      };

      const header: any = {
        Authorization: "Bearer " + token
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders(header);
        })
        .all("business/text/" + optId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
