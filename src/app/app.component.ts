import {
  Component,
  ChangeDetectorRef,
  OnInit,
  ViewEncapsulation
} from "@angular/core";
import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationEnd
} from "@angular/router";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { ElectronService } from "ngx-electron";
import * as $ from "jquery";

@Component({
  selector: "app-root",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "narnoo-admin-desktop";
  appClass: string;

  private subscription: Subscription;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _electronService: ElectronService
  ) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((res: any) => {
        const routeData: any = this.route.snapshot.firstChild.data;
        this.appClass = routeData.class;
        if (res.url !== "/login") {
          $("body").addClass("o-page");
          $("body").removeClass("o-page--center");
        } else {
          $("body").addClass("o-page");
          $("body").addClass(this.appClass);
        }
      });
  }

  ngOnInit() {
    if (this._electronService.isElectronApp) {
      const pong: string = this._electronService.ipcRenderer.sendSync(
        "synchronous-message",
        "ping"
      );
      console.log("pong: ", pong);
    }
  }
}
