import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationEnd
} from "@angular/router";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { NgxCoolDialogsService } from "ngx-cool-dialogs";
import { BroadcasterService } from "ng-broadcaster";

@Component({
  selector: "app-headers",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeadersComponent implements OnInit {
  hasSearch: boolean;
  appModule: string;
  txtSearch: string;
  currentFeature: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private coolDialogs: NgxCoolDialogsService,
    private broadcaster: BroadcasterService
  ) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((res: any) => {
        const routeData: any = this.route.snapshot.firstChild.data;
        console.log("routeData: ", routeData);
        this.appModule = routeData.title;
        this.hasSearch = routeData.hasSearchbar;
        this.currentFeature = routeData.name;
      });
  }

  ngOnInit() {}

  openHome() {
    this.router.navigate(["/home"]);
  }

  logOutApp() {
    this.coolDialogs
      .confirm("Are you sure to logout application?")
      .subscribe(res => {
        if (res) {
          localStorage.removeItem("app.adminData");
          localStorage.removeItem("app.admintoken");
          localStorage.removeItem("admin.bus.token");
          localStorage.removeItem("admin.business");

          setTimeout(() => {
            this.router.navigate(["/login"]);
          }, 600);
        }
      });
  }

  private searchData() {
    if (this.currentFeature === "products") {
      this.broadcaster.broadcast("search::product", this.txtSearch);
    } else if (this.currentFeature === "media") {
      this.broadcaster.broadcast("search::media", this.txtSearch);
    } else if (this.currentFeature === "users") {
      this.broadcaster.broadcast("search::users", this.txtSearch);
    }
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      console.log("search");
      this.searchData();
    }
  }
}
