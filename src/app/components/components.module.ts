import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BsDropdownModule, TooltipModule } from "ngx-bootstrap";
import { FormsModule } from "@angular/forms";

import { SidebarComponent } from "./sidebar/sidebar.component";
import { HeadersComponent } from "./header/header.component";
import { DashHeadersComponent } from "./dash-headers/dash-headers.component";
import { SkeletonItemComponent } from "./skeleton-item/skeleton-item.component";
import { MapViewComponent } from "./map-view/map-view.component";

@NgModule({
  declarations: [
    SidebarComponent,
    HeadersComponent,
    DashHeadersComponent,
    SkeletonItemComponent,
    MapViewComponent
  ],
  imports: [CommonModule, BsDropdownModule, TooltipModule, FormsModule],
  exports: [
    SidebarComponent,
    HeadersComponent,
    DashHeadersComponent,
    SkeletonItemComponent,
    MapViewComponent
  ]
})
export class ComponentsModule {}
