import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashHeadersComponent } from './dash-headers.component';

describe('DashHeadersComponent', () => {
  let component: DashHeadersComponent;
  let fixture: ComponentFixture<DashHeadersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashHeadersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashHeadersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
