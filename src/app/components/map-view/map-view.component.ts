import {
	Component,
	OnInit,
	ViewEncapsulation,
	Input,
	ElementRef,
	ViewChild,
	OnChanges,
	NgZone,
	SimpleChanges
} from "@angular/core";

declare const google: any;

@Component({
	selector: "app-map-view",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "./map-view.component.html",
	styleUrls: ["./map-view.component.css"]
})
export class MapViewComponent implements OnInit, OnChanges {
	@Input() latitude: number;
	@Input() longitude: number;
	@Input() title: string;
	@Input() imageSrc: any;

	map: any;

	@ViewChild("map_canvas") mapElement: ElementRef;
	constructor(private zone: NgZone) {}

	ngOnInit() {
		this.loadInteractiveMap(
			this.latitude,
			this.longitude,
			this.title,
			this.imageSrc
		);
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes.latitude) {
			if (
				changes.latitude.currentValue &&
				!changes.latitude.firstChange
			) {
				this.loadInteractiveMap(
					this.latitude,
					this.longitude,
					this.title,
					this.imageSrc
				);
			}
		}
	}

	private loadInteractiveMap(latitude, longitude, title?, imageSrc?) {
		console.log("loadInteractiveMap");

		function CustomMarker(latlng, map, _imageSrc) {
			var self = this;
			this.latlng_ = latlng;
			this.imageSrc = _imageSrc;
			this.setMap(map);
		}

		if (typeof google === "object" && typeof google.maps === "object") {
			CustomMarker.prototype = new google.maps.OverlayView();

			CustomMarker.prototype.draw = function() {
				// Check if the div has been created.
				let div = this.div_;
				if (!div) {
					// Create a overlay text DIV
					div = this.div_ = document.createElement("div");
					// Create the DIV representing our CustomMarker
					div.className = "customMarker";

					const img = document.createElement("img");
					img.src = this.imageSrc;
					div.appendChild(img);
					const me = this;
					google.maps.event.addDomListener(div, "click", function(
						event
					) {
						google.maps.event.trigger(me, "click");
					});

					// Then add the overlay to the DOM
					const panes = this.getPanes();
					panes.overlayImage.appendChild(div);
				}

				// Position the overlay
				const point = this.getProjection().fromLatLngToDivPixel(
					this.latlng_
				);
				if (point) {
					div.style.left = point.x + "px";
					div.style.top = point.y + "px";
				}
			};

			CustomMarker.prototype.remove = function() {
				// Check if the overlay was on the map and needs to be removed.
				if (this.div_) {
					this.div_.parentNode.removeChild(this.div_);
					this.div_ = null;
				}
			};

			CustomMarker.prototype.getPosition = function() {
				return this.latlng_;
			};

			const latLng = new google.maps.LatLng(latitude, longitude);
			const mapOptions2 = {
				center: latLng,
				zoom: 15,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				disableDefaultUI: true,
				draggable: false
			};
			this.map = new google.maps.Map(
				this.mapElement.nativeElement,
				mapOptions2
			);
			if (this.imageSrc) {
				const marker = new CustomMarker(
					latLng,
					this.map,
					this.imageSrc
				);
				this.addInfoWindow(marker, title);
			} else {
				const markerOptions: any = {
					position: latLng,
					title: title,
					map: this.map,
					animation: google.maps.Animation.DROP
				};
				const marker = new google.maps.Marker(markerOptions);
				this.addInfoWindow(marker, title);
			}
		}
	}

	private addInfoWindow(marker, content) {
		const infoWindow = new google.maps.InfoWindow({
			content
		});
		google.maps.event.addListener(marker, "click", () => {
			infoWindow.open(this.map, marker);
		});
	}
}
