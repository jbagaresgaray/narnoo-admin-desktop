import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationEnd
} from "@angular/router";
import * as _ from "lodash";

import { UtilitiesService } from "../../services/utilities.service";
import { BroadcasterService } from "ng-broadcaster";

@Component({
  selector: "app-sidebar",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  users: any = {};
  business: any = {};
  businessName: string;
  profilePicture: string;

  isMinimized = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private broadcaster: BroadcasterService,
    private utilities: UtilitiesService
  ) {}

  ngOnInit() {
    this.getUser();
  }

  private getUser() {
    this.business = JSON.parse(localStorage.getItem("admin.business")) || {};
    if (this.business.avatar) {
      const avatarUrl = this.business.avatar.image200.startsWith("//")
        ? "http:" + this.business.avatar.image200
        : this.business.avatar.image200;

      this.profilePicture = avatarUrl;
    } else {
      this.profilePicture = this.utilities.radomizeAvatar();
    }
    this.businessName = !_.isEmpty(this.business.name)
      ? this.business.name
      : this.business.business;
  }

  activeRoute(routename: string): boolean {
    return this.router.url.indexOf(routename) > -1;
  }

  showSidebar() {
    this.isMinimized = !this.isMinimized;
    console.log("this.isMinimized: ", this.isMinimized);
    this.broadcaster.broadcast("js-page-sidebar", this.isMinimized);
  }

  openDashboard() {
    this.router.navigate(["/dashboard"]);
  }

  openMedia() {
    this.router.navigate(["/media"]);
  }

  openCompany() {
    this.router.navigate(["/company-bio"]);
  }

  openProducts() {
    this.router.navigate(["/products"]);
  }

  openImportProducts() {
    this.router.navigate(["/import-products"]);
  }

  openUsers() {
    this.router.navigate(["/users"]);
  }

  openSocialLinks() {
    this.router.navigate(["/social-links"]);
  }

  openUserProfile() {
    this.router.navigate(["/user-profile"]);
  }
}
