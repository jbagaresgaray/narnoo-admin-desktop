"use strict";

const { app, BrowserWindow, globalShortcut, Menu } = require("electron");
const url = require("url");
const path = require("path");
const menuBuilder = require("./scripts/menu");

let win, serve;
const args = process.argv.slice(1);
serve = args.some(val => val === "--serve");
console.log("serve: ", serve);

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1024,
    height: 800,
    backgroundColor: "#ffffff",
    icon: path.join(__dirname, "assets/icons/png/64x64.png"),
    webPreferences: {
      nodeIntegration: true
    },
    title: "Narnoo Admin Desktop"
  });
  // win.setMenu(null);
  require("./scripts/events")(app, win);

  if (serve) {
    require("electron-reload")(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL("http://localhost:4200");
    win.webContents.openDevTools();
    require("devtron").install();
  } else {
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file:",
        slashes: true
      })
    );
  }

  //// uncomment below to open the DevTools.
  menuBuilder.buildMenu();
  globalShortcut.register("CmdOrCtrl+Shift+D", () => {
    win.webContents.toggleDevTools();
  });

  // Event when the window is closed.
  win.on("closed", function() {
    win = null;
  });
}

// Create window on electron intialization
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", function() {
  globalShortcut.unregisterAll();
  // On macOS specific close process
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function() {
  // macOS specific close process
  if (win === null) {
    createWindow();
  }
});
